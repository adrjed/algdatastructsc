/* Laboratoria 1 - 6.10.2014 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "ajextra.h"
#include "Tables.h"
#include "Lists.h"

#include "diag.h"

/*
 * #ifdef DIAG - tryb pomiaru czasu wykonaania kodu
 * #ifndef DIAG - tryb użytkownika
 */
//#undef DIAG

/* Lab 1.1. (1) Na wejściu znajduje sie ilość liczb, a następnie liczby.
 *	Utwórz z tych danych listę jednokierunkową oraz wyślietl tą listę w
 *	normalnej i odwrotnej kolejności.
 *
 * Funkcje stworzone w tym zadaniu :
 *	List_Print
 *	List_PrintNeg
 *	List_New
 */

#ifdef DIAG
List Lab1Zad1(int length, int data[])
{
	List listInt = 0;

	DIAG_CLOCK_START
	if (length > 0)
		listInt = List_New(length, data);
	else
		listInt = 0;
	DIAG_CLOCK_STOP

	return listInt;
}
#else
List Lab1Zad1(int length, int data[])
{
	List listInt = 0;

	printf("\n1.1. Wynik:");
	if (length > 0)
		listInt = List_New(length, data);
	else
		listInt = 0;
	printf("\nDrukowanie normalnie: \n");
	List_Print(listInt);
	printf("\nDrukowanie odwrotne: \n");
	List_PrintReversed(listInt);
	return listInt;
}
#endif

/* Lab 1.2. (2) Dla listy z poprzedniego zadania usuń wszystkie
 *	wartości parzyste i wyświetl zawartość listy
 *
 * Funkcje stworzone w tym zadaniu : 
 *	List_DeleteEvenNumbers
 */
#ifdef DIAG
void Lab1Zad2(List *l)
{
	DIAG_CLOCK_START
	List_DeleteEvenNumbers(l);
	DIAG_CLOCK_STOP
}
#else
void Lab1Zad2(List *l)
{
	printf("\n1.2. Wynik:\n");
	List_DeleteEvenNumbers(l);
	List_Print(*l);
}
#endif

/* Lab 1.3. (3) Na wejściu podana jest liczba łańcuchów znakowych,
 *	a następnie łańcuchy znakowe. Zbuduj dwie listy. Pierwsza z nich to
 *	lista składająca się z palindromów, a druga z pozostałych.
 *
 * Palindrom = wyraz symetryczny względem środka( LABAL, KAJAK, źle 12A22 )
 *
 * Funkcje stworzone w tym zadaniu :
 *	isPalindrome
 *	GetRandom
 *	GetPalindrome
 *	GetRandomString
 *	StrTab_GenTestTab
 */
int isPalindrome(char *str)
{
	int length = strlen(str), i = 0;

	for (; i < length / 2; i++)
		if (str[i] != str[length - 1 - i])
			return 0;
	return 1;
}

char* GetPalindrome(int length)
{
	int i = 0;
	char *c = 0;

	c = (char*)malloc(sizeof(char)*length+1);
	c[length] = 0;
	for (; i < length / 2; i++)
		c[i] = c[length - i - 1] = int_RandMM((int)'a', (int)'z');

	return c;
}

char* GetRandomString(int length)
{
	int i = 0;
	char *c = 0;

	c = (char*)malloc(sizeof(char)*length + 1);
	c[length] = 0;
	for (; i < length; i++)
		c[i] = int_RandMM((int)'a', (int)'z');

	return c;
}

StrTab* StrTab_GenTestTab(int length, int strMaxLen)
{
	//int currStrLength = 0, i = 0;
	StrTab *strTab = (StrTab*)malloc(sizeof(StrTab));
	strTab->length = length;
	strTab->string = (char**)malloc(sizeof(char*)*length);

	for (int i=0; i < length; i++)
	{
		if (rand() % 2)
			strTab->string[i] = GetPalindrome(length);
		else
			strTab->string[i] = GetRandomString(length);
	}

	return strTab;
}

#ifdef DIAG
void Lab1Zad3(int strCount, char **strTab)
{
	StrList listPalindroms = 0;
	StrList listNotPalindroms = 0;

	DIAG_CLOCK_START
	for (int i = 0; i < strCount; i++)
	{
		if (isPalindrome(strTab[i]))
			StrList_Add(&listPalindroms, strTab[i]);
		else
			StrList_Add(&listNotPalindroms, strTab[i]);
	}
	DIAG_CLOCK_STOP

	StrList_Free(listPalindroms);
	StrList_Free(listNotPalindroms);
}
#else
void Lab1Zad3(int strCount, char **strTab)
{
	StrList listPalindroms = 0;
	StrList listNotPalindroms = 0;

	for (int i = 0; i < strCount; i++)
	{
		if (isPalindrome(strTab[i]))
			StrList_Add(&listPalindroms, strTab[i]);
		else
			StrList_Add(&listNotPalindroms, strTab[i]);
	}
	printf("\n1.3. Wynik:");
	printf("\nPalindromy:\n");
	StrList_Print(listPalindroms);
	printf("\nPozostale:\n");
	StrList_Print(listNotPalindroms);

	StrList_Free(listPalindroms); StrList_Free(listNotPalindroms);
}
#endif

#ifdef DIAG
int Lab1Main(int argc, char**argv)
{
	List 	l = 0;
	StrTab 	*s = 0;
	int 	*it = 0;
	FILE *f = 0;

	int STEPS = 100,
		START = 0,
		REPEATS = 2;
	float MULTIPLER =  1000.0;

	printf("/* Laboratorium 1 - 6.10.2014 - Adrian Jedrzejak (267531) */\n");
	printf("/* Pomiar zlozonosci czasowej dla ustalonej serii danych /*\n\n");

	/* Zadanie 1 - badanie złożoności kodu */
	Diag_ConfigTest(&START, &MULTIPLER, &STEPS, &REPEATS);
	f =  Diag_OpenFile("Lab1Zad1Diag.csv");
	printf("\n== POMIAR DLA ZADANIA 1.1. ==\n");
	DIAG_DATA_LOOP_START(START, MULTIPLER, STEPS, REPEATS)
		it = IntTab_New_Asc(DiagDataLength, 1);

		l = Lab1Zad1(DiagDataLength, it); // ZADANIE 1.1.

		DIAG_CHECK_CLOCK
		List_Free(l);free(it);

		DIAG_DATA_SERIES_END
		printf("1.1. Step: %2d Length: %-10d Time: %-7.2f ms\n",DiagStep, DiagDataLength, DIAG_CHECK_AVERAGE);
		Diag_AddResult(f, DiagInfo);
	DIAG_DATA_LOOP_END
	Diag_CloseFile(f);
	printf("\n== KONIEC POMIARU DLA ZADANIA 1.1. ==\n");

	/* Zadanie 2 - badanie złożoności kodu */
	Diag_ConfigTest(&START, &MULTIPLER, &STEPS, &REPEATS);
	f = Diag_OpenFile("Lab1Zad2Diag.csv");
	printf("\n== POMIAR DLA ZADANIA 1.2. ==\n");
	DIAG_DATA_LOOP_START(START, MULTIPLER, STEPS, REPEATS)
		it = IntTab_New_Asc(DiagDataLength, 1);
		l = Lab1Zad1(DiagDataLength, it);

		Lab1Zad2(&l); // ZADANIE 1.2.

		DIAG_CHECK_CLOCK
		List_Free(l);free(it);

		DIAG_DATA_SERIES_END
		printf("1.2. Step: %2d Length: %-10d Time: %-7.2f ms\n",DiagStep, DiagDataLength, DIAG_CHECK_AVERAGE);
		Diag_AddResult(f, DiagInfo);
	DIAG_DATA_LOOP_END
	Diag_CloseFile(f);
	printf("\n== KONIEC POMIARU DLA ZADANIA 1.2. ==\n");

	/* Zadanie 3 - badanie złożoności kodu */
	Diag_ConfigTest(&START, &MULTIPLER, &STEPS, &REPEATS);
	printf("\n== POMIAR DLA ZADANIA 1.3. ==\n");
	f = Diag_OpenFile("Lab1Zad3Diag.csv");
	DIAG_DATA_LOOP_START(START, MULTIPLER, STEPS, REPEATS)
		s = StrTab_GenTestTab(DiagDataLength, 5);

		Lab1Zad3(s->length, s->string); // ZADANIE 1.3.

		DIAG_CHECK_CLOCK
		StrTab_Free(s);

		DIAG_DATA_SERIES_END
		if (DiagDataLength >= 3000)
			printf("Uwaga! Dlugi czas operacji\n");
		Diag_AddResult(f, DiagInfo);
		printf("1.3. Step: %2d Length: %-10d Time: %-7.2f ms\n",DiagStep, DiagDataLength, DIAG_CHECK_AVERAGE);
	DIAG_DATA_LOOP_END
	Diag_CloseFile(f);
	printf("\n== KONIEC POMIARU DLA ZADANIA 1.1. ==\n");

	printf("\nKoniec pomiarow Lab1 - nacisnij klawisz...");
	getchar();return 0;
}
#else
int Lab1Main(int argc, char **argv)
{
	int length =0, *data = 0;
	List l = 0;
	StrTab *st = (StrTab*)malloc(sizeof(StrTab));

printf("/* Laboratorium 1 - 6.10.2014 - Adrian Jedrzejak (267531) */\n\n");

	// ZADANIE 1.1.
printf("\
/* Lab 1.1. (1) Na wejsciu znajduje sie ilosc liczb, a nastepnie liczby.\n\
 *	Utworz z tych danych liste jednokierunkowa oraz wywietl liste w\n\
 *	normalnej i odwrotnej kolejnosci.\n\
 */\n\n");
printf("> Podaj ilosc liczb do wprowadzenia: "); scanf(" %d", &length); 
fflush(stdin);
printf("> Podaj liczby: "); data = IntTab_New_Scanf(length); 
fflush(stdin);

	l = Lab1Zad1(length, data);
	free(data);

	//printf("\n\n>> Przejdz do 2.2. - nacisnij klawisz... \n"); getchar();

	// ZADANIE 1.2.
printf("\n\n\
/* Lab 1.2. (2) Dla listy z poprzedniego zadania usun wszystkie\n\
 *	wartosci parzyste i wyswietl zawartosc listy\n\
 */");
printf("\n> Pobranie wynikow z 2.1. do 2.2. ...\n");

	Lab1Zad2(&l);
	List_Free(l);

	//printf("\n\n>> Przejdz do 2.3. - nacisnij klawisz... \n"); getchar();

	// ZADANIE 1.3.
printf("\n\n\
/* Lab 1.3. (3) Na wejsciu podana jest liczba lancuchow znakowych,\n\
 *	a nastepnie lancuchy znakowe. Zbuduj dwie listy. Pierwsza z nich to\n\
 *	lista skladajaca sie z palindromow, a druga z pozostalych.\n\
 *\n\
 * Palindrom = wyraz symetryczny wzgledem srodka ( LABAL, KAJAK )\n\
 */\n\n");
printf("> Ile wyrazow chcesz podac: "); scanf(" %d", &(st->length)); 
fflush(stdin);
printf("> Podaj wyrazy: "); st->string = StrTab_Scanf(st->length); 
fflush(stdin);

	Lab1Zad3(st->length, st->string);
	StrTab_Free(st);

printf("\n>> Koniec Lab 1 - nacisnij klawisz...");
fflush(stdin); getchar();
	return 0;
}
#endif
