/**
 * @file ajextra.h
 * @author Adrian Jędrzejak
 * @brief Funkcje i makra wspomagające debugowanie i poprawiające czytelność kodu
 *	oraz pewne funkcje rozszerzające
 */

#ifndef AJ_EXTRA_H
#define AJ_EXTRA_H

/**
 * Sprawdza czy wskaźnik jest pusty
 */
#define ifnull(x) if((x)==0)

/**
 * Sprawdza czy wskaźnik nie jest pusty
 */
#define ifnotnull(x) if((x)!=0)

#define DEBUG

#ifdef DEBUG
#define DEBUG_TEST 1
#else
#define DEBUG_TEST 0
#endif

/**
 * Wyświetla informację w konsoli, jeśli DEBUG 1
 * Pozwala na wyświetlenie dodatkowych zmiennych
 */

/**
 * Wyświetla informacje w konsoli, jeśli DEBUG 1
 * Pozwala wyświetlić tylko tekst
 */
#ifdef LINUX
#define debug_print(fmt, ...) \
        do { if (DEBUG_TEST) fprintf(stderr, "%s:%d:%s(): " fmt, __FILE__, \
                                __LINE__, __func__, ##__VA_ARGS__); } while (0)
#else
#define debug_print(fmt) \
		do { if (DEBUG_TEST) fprintf(stderr, "%s:%s(): ", fmt); } while (0)
#endif
/**
 * Zwraca losową wartość całkowitą z przedziału [min,max]
 * @param min - minimalna wartość
 * @param max - maksymalna wartość
 * @return - wylosowana wartość
 */
int int_RandMM(int min, int max);

/**
 * Czyści bufor wejściowy ze znaków do czytania
 */
void stdin_Clear();

#endif//AJ_EXTRA_H
