#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#include "Tables.h"
#include "ajextra.h"

/**
 * Generates array of ints in ascending order
 * @param length
 * @param startValue
 * @return - pointer on int array
 */
int* IntTab_New_Asc(int length, int startValue)
{
	int *intTab = (int*)malloc(sizeof(int)*length), i = 0;
	for (; i<length; i++)
		intTab[i] = startValue++;
	return intTab;
}

int* IntTab_New_Desc(int length, int startValue)
{
	int *intTab = (int*)malloc(sizeof(int)*length), i = 0;
	for (; i < length; i++)
		intTab[i] = startValue--;
	return intTab;
}

int* IntTab_New_Rand(int length, int min, int max)
{
	int *intTab = (int*)malloc(sizeof(int)*length), i = 0;
	for(; i < length; i++)
		intTab[i] = int_RandMM(min, max);
	return intTab;
}

int* IntTab_New_Scanf(int length)
{
	int *intTab = (int*)malloc(sizeof(int)*length), i = 0;
	for (; i < length; i++)
		scanf(" %d", intTab + i);
	return intTab;
}

float* FloatTab_New_Scanf(int length)
{
	float *floatTab = (float*)malloc(sizeof(float)*length);
	int i = 0;
	for(; i < length; i++)
		scanf(" %f", floatTab + i);

	return floatTab;
}

uint* UIntTab_New_Scanf(int length)
{
	uint *uiTab = (uint*)malloc(sizeof(uint)*length);
	for( int i = 0; i  < length; i++)
		scanf(" %u", uiTab + i);

	return uiTab;
}

uint* UIntTab_CS_Scanf(int length, uint *min,  uint *max)
{
	uint ma = *max = 0, val = 0;
	uint mi = *min = UINT_MAX;
	uint *uiTab = (uint*)malloc(sizeof(uint)*length);
	for( int i = 0; i < length; i++)
	{
		scanf(" %u", &val);
		if( val > ma ) ma = *max = val;
		if( val < mi ) mi = *min = val;
		uiTab[i] = val;
	}
	return uiTab;
}
// Ta wersja ma jakieś problemy
void UIntTab_OldCountingSort(uint uitab[], int uilen, uint min, uint max)
{
	int len = max-min+1;
	uint *uitabc = (uint*)calloc((len), sizeof(uint));
	uint *uitabs = (uint*)calloc((uilen), sizeof(uint));

	for( int i = 0; i < len; i++)
		uitabc[uitab[i]-min]++; //zwiększamy licznik wartości dla wartości pod uitab[i]
								//pamiętając żeby przesunąć sie o wartość minimalną

	for( int i = min+1; i <= max; i++)
		uitabc[i-min] += uitabc[i - min - 1];

	for( int i = uilen-1; i >= 0; i--)
		uitabs[(uitabc[uitab[i] - min]--) - 1 ] = uitab[i];

}
// Ta wersja wydaje się działać, ale możliwe że wykonuje więcej pracy, nie wiem
void UIntTab_CountingSort(uint *uitab, int uilen, uint min, uint max)
{
	int len = max-min+1, i = 0, j = 0;
	// tablica liczników wartości o rozpiętości [min, max]
	uint	*uitabc = (uint*)calloc(len, sizeof(uint));

	// zliczamy wartości w tablicy
	for( i = 0; i < uilen; i++)
		uitabc[uitab[i]-min]++;

	for( i = 0; i < len; i++)
	{
		while( uitabc[i]-- > 0 )
			uitab[j++] = i+min;
	}
	free(uitabc);
}
// SORTOWANIE PRZEZ KOPCE - implementacja za pomocą tablicy

void FloatTab_Heapify(float ft[], int n , int fsize)
{
	int l = 2*n + 1;	// lewe "poddrzewo"
	int r = 2*n + 2;	// prawe "poddrzewo"
	float x = 0; int largest = 0 ;

	if ( l < fsize && ft[l] > ft[n])
		largest = l;
	else
		largest = n;
	if (r < fsize && ft[r] > ft[largest])
		largest = r;
	if ( largest != n)
	{
		x = ft[n];
		ft[n] = ft[largest];
		ft[largest] = x;
		FloatTab_Heapify(ft, largest , fsize );// rekurencja
	}
}

void FloatTab_BuildHeap(float ft[], int fsize)
{
	for ( int i = (fsize - 1) / 2; i >= 0; i--)
		FloatTab_Heapify( ft, i , fsize);
}

void FloatTab_HeapSort(float ft[], int fsize)
{
	float x;
	int heapSize = fsize;
	FloatTab_BuildHeap(ft, fsize);
	for( int i = fsize-1; i > 0; i--)
	{
		x = ft[0];
		ft[0] = ft[i];
		ft[i] = x;
		FloatTab_Heapify(ft, 0, --heapSize);
	}
}



/* strTab type */

char** StrTab_Scanf(int count)
{
	char **strTab = (char**)malloc(sizeof(char*)*count);
	char buff[256] = { '\0' };
	int i = 0;

	for (; i < count; i++)
	{
		scanf(" %s", buff);
		strTab[i] = (char*)malloc(sizeof(char)*strlen(buff) + 1);
		strcpy(strTab[i], buff);
	}
	return strTab;
}

void StrTab_Free(StrTab *s)
{
	int i = 0;
	for (; i < s->length; i++)
		free(s->string[i]);
	free(s);
}

void StrTab_Print(int length, char **strTab)
{
	int i = 0;
	for (i = 0; i < length; i++)
		printf("%s\n", strTab[i]);
}
