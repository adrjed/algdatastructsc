#include <stdlib.h>
#include <stdio.h>

#include "diag.h"

#ifdef _WIN32
#include <Windows.h>
#elif __linux
#include <time.h>
#endif

void Diag_StartClock(Diag d){
#ifdef _WIN32
	QueryPerformanceCounter(&(d->start));
#elif __linux
	clock_gettime(CLOCK_REALTIME, &(d->start));
#endif
}
void Diag_StopClock(Diag d){
#ifdef _WIN32
	QueryPerformanceCounter(&(d->end));
	QueryPerformanceFrequency(&(d->frequency));

	d->elapsed.ns = (double)(d->end.QuadPart - d->start.QuadPart) / d->frequency.QuadPart;
	// w jednym * 1000.0, w drugim / 1000.0 ???
	d->elapsed.ms = d->elapsed.ns * 10000.0;
#elif __linux
	clock_gettime(CLOCK_REALTIME,&(d->end));
	clock_getres(CLOCK_REALTIME, &(d->frequency));

	d->elapsed.ns = (double)(d->end.tv_nsec - d->start.tv_nsec) ;/// d->frequency.tv_nsec;
	d->elapsed.ms = d->elapsed.ns / 1000.0;
#endif
}

void Diag_Reset(Diag d){
	d->elapsed.ms = 0;
	d->elapsed.ns = 0;
	//d->start = 0;
	//d->end = 0;
}

void Diag_ConfigTest(int *startLength, float *multipler, int *series, int *serieRepeats)
{
	char isChange = 0;

	printf("Dlugosci poczatkowa:\t %d\n", *startLength);
	printf("Dodawana stala:\t\t %.2f\n", *multipler);
	printf("Liczba serii:\t\t %d\n", *series);
	printf("Liczba powtorzen serii:\t %d\n", *serieRepeats);
	do
	{
		printf("Zmienic parametry ? [y/N]: "); scanf(" %c", &isChange);
		fflush(stdin);
	} while (isChange != 'y' && isChange != 'Y' && isChange != 'n' && isChange != 'N');

	if (isChange == 'y' || isChange == 'Y')
	{
		printf("Dlugosci poczatkowa:\t "); scanf(" %d", startLength);
		fflush(stdin);
		printf("Dodawana stala:\t\t "); scanf(" %f", multipler);
		fflush(stdin);
		printf("Liczba serii:\t\t "); scanf(" %d", series );
		fflush(stdin);
		printf("Liczba powtorzen w serii:\t "); scanf(" %d", serieRepeats);
		fflush(stdin);
	}
	else
		printf("Kontynuowanie bez zmian");
}

FILE* Diag_OpenFile(char *name)
{
	FILE *f = fopen(name, "w");
	fprintf(f, "Step \t Repeats \t Length \t Time[ms]:\n");
	return f;
}

void Diag_CloseFile(FILE *f)
{
	fclose(f);
	f = 0;
}

void Diag_AddResult(FILE* f, diagInfo_t di)
{
	fprintf(f, "%d, %d, %d, %.4f\n", di.step, di.repeats, di.length, di.elapsed.ms);
}

int Diag_Random(int min, int max)
{
	int tmp;
	if (max >= min)
		max -= min;
	else
	{
		tmp = min - max;
		min = max;
		max = tmp;
	}
	return max ? (rand() % max + min) : min;
}
