/**
 * @file Laboratoria.h
 * @author Adrian Jędrzejak
 * @brief Rozwiązania do zadań z pierwszych laboratoriów
 */

#ifndef AJ_LABORATORIA_H
#define AJ_LABORATORIA_H

int Lab1Main(int argc, char**argv);

int Lab2Main(int argc, char**argv);

int Lab3Main(int argc, char**argv);

int Lab4Main(int argc, char**argv);

int LabLMain(int argc, char**argv);

//void Lab4Main(int argc, char**argv);

#endif//AJ_LABORATORIA_H
