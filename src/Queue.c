#include <stdlib.h>
#include <stdio.h>

#include "Queue.h"

QueuePElem* QueuePElem_New(int value , int priority, QueuePElem* next)
{
	QueuePElem* qp = (QueuePElem*)malloc(sizeof(QueuePElem));
	qp->value = value;
	qp->priority = priority;
	qp->next = next;
	return qp;
}

void QueueP_Push(QueueP *qp, int value, int priority)
{
	QueuePElem  *q = *qp, *qprev = 0;

	if (!q)
	{
		*qp = QueuePElem_New(value, priority, 0); return;
	}
	while (q)
	{
		if (q->priority >= priority)
		{
			qprev = q;
			q = q->next;
		}
		else
			break;
	}
	if (qprev)
		qprev->next = QueuePElem_New(value, priority, q);
	else
		*qp = QueuePElem_New(value, priority, *qp);
}

void QueueP_PushElem(QueueP *qp, QueuePElem *qpe)
{
	QueuePElem *q = *qp, *qprev = 0;
	if(!q)
		*qp = qpe;
	else
	{
		while(q)
		{
			if(q->priority >= qpe->priority)
			{
				qprev = q;
				q = q->next;
			}
			else
				break;
		}
		if(qprev)
			qprev->next = qpe;
		else
			*qp = qpe;
		qpe->next = q;
	}
}

QueuePElem* QueueP_Pop(QueueP *qp)
{
	if (!(*qp)) return 0;
	QueuePElem *qe = *qp;
	*qp = (*qp)->next;
	return qe;
}

QueuePElem* QueueP_PopElem(QueueP *qp, int value, int priority)
{
	QueueP q = *qp, result=0, prev=0;
	if(q==0) return 0;
	while(q)
	{
		prev = result; result = q;
		if(q->value == value && q->priority == priority)
			break;
		q = q->next;
	}

	if(prev == 0)
		return QueueP_Pop(qp);
	else
	{
		prev->next = result->next;
		return result;
	}
}

void QueueP_SetPriority(QueueP *qp,int value, int priority, int newPriority)
{
	QueuePElem* qpe = QueueP_PopElem(qp, value, priority);
	qpe->priority = newPriority;
	QueueP_PushElem(qp, qpe);
}

void QueueP_Print(QueueP qp)
{
	while (qp)
	{
		printf(" [%d,%d]", qp->value, qp->priority);
		qp = qp->next;
	}
}
