/* Ćwiczenia 2 - 10.10.2014 */
#include <stdlib.h>
#include <stdio.h>

#include "Lists.h"
#include "Tables.h"

#define TAB_LEN 20

/*
 * Ćw 2.1. (7) Utworzyć listę liczb całkowitych i usunąć parzyste liczby
 */
void Cw2Zad1()
{
	int* it = IntTab_New_Asc(TAB_LEN, TAB_LEN);
	List l = List_New(TAB_LEN, it);
	List prev = 0, curr = l, tmp = 0;
	free(it);

	printf("Przed usuwaniem parzystych:\n");
	List_Print(l);

	while (curr)
	{
		if (curr->value % 2 == 0)
		{
			if (prev)
				prev->next = curr->next;
			tmp = curr->next;
			free(curr);
			curr = tmp;
		}
		else
		{
			prev = curr;
			curr = curr->next;
		}
	}

	printf("Po usunieciu parzystych:\n");
	List_Print(l);
	List_Free(l);
}

/* 
 * Ćw 2.2. (8) Utworzyć liste liczb rzeczywistych i policzyć sumę
 */
typedef struct _fList{
	float value;
	struct _fList *next;
} FListElem;
typedef FListElem* FList;

FList FList_New(int length, float data[])
{
	FList fl = 0, l = 0;
	int i = 0;
	if (length <= 0 || !data) return 0;
	else
	{
		fl = (FList)malloc(sizeof(FListElem));
		fl->value = data[i];
		fl->next = 0;
		i++;
	}
	l = fl;
	for (; i < length; i++)
	{
		l->next = (FList)malloc(sizeof(FListElem));
		l->next->value = data[i];
		l->next->next = 0;
		l = l->next;
	}
	return fl;
}

float FList_Sum(FList fl)
{
	float sum = 0;
	int count = 0;
	while (fl)
	{
		sum += fl->value;
		count++;
		fl = fl->next;
	}
	return sum / (float)count;
}

void FList_Print(FList fl)
{
	while (fl)
	{
		printf("%f \n", fl->value);
		fl = fl->next;
	}
}

float* flTab_Gen(int length)
{
	float* f = (float*)malloc(sizeof(float));
	float step = 1.25f;
	while (length--)
	{
		f[length] = step;
		step += 1;
	}
	return f;
}

void Cw2Zad2()
{
	FList fl = FList_New(TAB_LEN, flTab_Gen(TAB_LEN));

	FList_Print(fl);
	printf("suma elementow: %f", FList_Sum(fl));
}

/*
 * Ćw 2.3. (9) Funkcja sprawdzająca ilość wystąpień najmniejszego elementu
 */
int List_MinCount(List l)
{
	int min = 0,
		count = 0;

	if (!l) return 0;
	min = l->value;
	count = 1;
	l = l->next;

	while (l)
	{
		if (l->value < min)
		{
			min = l->value;
			count = 1;
		}
		else if (l->value == min)
			count++;
		
		l = l->next;
	}
	return count;
}

void Cw2Zad3()
{
	List l = List_New(5, IntTab_New_Scanf(5));
	int minCount = List_MinCount(l);

	printf("Liczba minimalnych: %d", minCount);
}

int Cw2Main(int argc, char **argv)
{
	//Cw2Zad1();
	//Cw2Zad2();
	//Cw2Zad3();
	getchar();
	return 0;
}
