/**
 * Algorytmy i Struktury Danych
 * Lista zadań z laboratorium
 * @date 2014/15r.
 * @author Adrian Jędrzejak
 * @mail 267531@fizyka.umk.pl
 * @authors Norbert Jankowski, Miłosz Michalski
 * @link http://www.is.umk.pl/~norbert/asd/lab-zadania.pdf
 */

#include <stdio.h>

#include "ajlogo.h"
#include "ajextra.h"
#include "command.h"

#include "LabLista.h"
#include "Graphs.h"
#include "Lists.h"
#include "ONP.h"
#include "Queue.h"
#include "Stack.h"
#include "Tables.h"
#include "TreeFS.h"
#include "Trees.h"

#ifdef LINUX
#include <unistd.h>
#endif

void LabLGlobInfo(){
	printf("-e \t\t - exit");
}

/*	1. Program czyta liczby całkowite podawane z klawiatury aż do pojawienia się wartości ujemnej.
 *	Należy utwotrzyć z nich nieposortowaną listę prostą dołączając kolejne elementy a) na początku,
 *	b) na końcu listy. Następnie uzupełnić program o procedury drukowania zawartości listy w porządku
 *	prostym i odwrotnym.
*/
#define LLZ1_A
void LabLZad1()
{
	int num = 0;
	List l = 0;
	puts("Podaj wartości i wybierz ENTER");
	do {
		scanf(" %d", &num);
#ifdef LLZ1_A
		List_Add(&l, num);
#elif LLZ1_B
		List_AddToEnd(&l, num);
#endif
	} while( !Cmd_IsEnter() );

	List_Print(l);
	puts("\n");
	List_PrintReversedRec(l);
	List_Free(l);
}

/*	2. Uzupełnić program z zad. 1 o funkcję znajdź(n, P) wyszukującą wartość n wśród elementów listy
 *	wskazywanej przez P . Funkcja ta powinna zwracać jako wartość wzkaźnik na odnaleziony element
 *	listy lub nil gdy go nie znaleziono. Przetestować ją wczytując różne wartości n i czytelnie wyświetlając wyniki przeszukania.
 *	Następnie zaimplementować operacje dodawania nowego elementu m
 *	a) przed b) za elementem wskazanym przez funkcję “znajdź”. Zaimplementować także operację
 *	c) usuwania wskazanego elementu z listy. Po dodaniu/usunięciu elementu zawartość listy należy kontrolnie wydrukować.
 *	Program powinien działać w pętli, tak aby można było wielokrotnie
 *	testować odnajdywanie, dopisywanie i usuwanie elementów, aż do całkowitego opróżnienia listy.
*/
void LabLZad2Info()
{
	puts("Opcje dla zadania 2:");
	printf("%s <int> \t- wyszukaj element\n", CmdList[CMD_FIND]);
	printf("%s -[a|b] <int> \t- dodaj przed [-b]/po [-a]/na koniec\n", CmdList[CMD_ADD]);
	printf("%s <int> \t- usun wartosci\n", CmdList[CMD_REMOVE]);
}
void LabLZad2()
{
	List l = 0;ListElem *le = 0;
	int num = 0;char opt = 0;

	puts("Podaj wartości i wybierz ENTER");
	List_Scanf(&l);

	while( opt != CMD_EXIT ) 	// wyszukiwanie elementow w liscie
	{	fflush(stdin);
		printf("LISTA: ");List_Print(l);putchar('\n');
		switch(opt = Cmd_Read(0))
		{
		case CMD_FIND:
			do { scanf(" %d", &num);
				le = List_Search(l, num);
				if(le)	printf("%d\t- Znaleziono %d\n", num, le->value);
				else	printf("%d\t- Nie znaleziono\n", num); }
			while( !Cmd_IsEnter() );
			break;
		case CMD_ADD: opt = Cmd_GetOpt();
			do { scanf(" %d", &num);
				if ( (opt == 'b' || opt == 'a') && !le ){
					printf("Nie wyszukano wartosci za pomoca find\n"); break; }
				if ( opt == 'b')
					List_AddBefore(&l, le, num);
				else if ( opt == 'a')
					List_AddAfter(&l, le, num);
				else
					List_AddToEnd(&l, num);
			} while( !Cmd_IsEnter() );
			break;
		case CMD_REMOVE:
			do {scanf(" %d", &num);
				if (List_RemoveValuesOf(&l, num))
					printf("Usunieto %d", num);
				else
					printf("Nie znaleziono %d", num);
			} while( !Cmd_IsEnter() && l );
			break;
		case CMD_HELP:
			LabLZad2Info(); break;
		case CMD_EXIT:
			puts("Konczenie programu..."); break;
		default:
			puts("Opcja nieprawidlowa");break;
		}
	}
	List_Free(l);
}
/*
3. Do programu dodać funkcję usuńparz(P) zwracająca wskaźnik na listę powstałą z P przez usunięcie
wszystkich liczb parzystych. Wydrukowac wynik.
*/
void LabLZad3Info()
{
	puts("Opcje dla zadania 3:");
	printf("%s \t- usun nieparzyste\n", CmdList[CMD_REMOVE]);
}
void LabLZad3()
{
	List l = 0; char opt = 0;
	puts("Podaj wartości i wybierz ENTER");
	List_Scanf(&l);

	while ( opt != CMD_EXIT )
	{	fflush(stdin);
		printf("LISTA: ");List_Print(l);putchar('\n');
		switch (opt = Cmd_Read(0))
		{
		case CMD_REMOVE:
			List_DeleteEvenNumbers(&l); break;
		case CMD_HELP:
			LabLZad3Info(); break;
		case CMD_EXIT:
			puts("Wyjscie..."); break;
		default:
			puts("Opcja nieprawidlowa");break;
		}
	}
	List_Free(l);
}
/*
4. Uzupełnić program z zadania 1 o procedurę odwracania listy bez generowania nowych elementów
(tj. wyłącznie przez manipulacje na łącznikach). Zaproponować oddzielnie rozwiązania iteracyjne
i rekursywne.
*/
void LabLZad4()
{
	List l = 0;
	puts("Podaj wartości i wybierz ENTER");
	List_Scanf(&l);

	puts("LISTA: ");List_Print(l);
	List_Reverse(&l);
	puts("\nREVERSE: ");List_Print(l);
	List_Free(l);
}
/*	5. Podobnie jak w zadaniu 1 tworzymy teraz listę posortowaną rosnąco. Zrealizować (i porównać)
2 rozwiązania: bez wartownika i z wartownikiem. Uzupełnić program o funkcję wyszukiwania
wskazanego elementu korzystającą z faktu uporządkowania listy.
*/
void LabLZad5()
{
	List l = 0;
	puts("Podaj wartości i wybierz ENTER");
	List_Scanf(&l);

	List_Sort(&l, 1);	// sortowanie listy rosnąco
	List_Print(l);
	List_Free(l);
}
/*
6. Usunąć z listy a) nieposortowanej, b) posortowanej powtarzające się wartości. 
Np. lista 2 → 3 → 1 → 3 → 4 → 2 ma zostać przetworzona na 2 → 3 → 1 → 4.
*/
#define LLZ6_B
void LabLZad6()
{
	List l = 0;
	puts("Podaj wartości i wybierz ENTER");
	List_Scanf_ToEnd(&l);

#ifdef LLZ6_A
	List_RemoveRepeated(l);
#endif
#ifdef LLZ6_B
	List_Sort(&l, 1);
	puts("LISTA ROSNACO: ");List_Print(l);
	List_Sorted_RemoveRepeated(l);
#endif
	puts("USUNIECIE POWTARZANYCH WARTOSCI: ");List_Print(l);
	List_Free(l);
}
/*
7. Korzystając z procedur zad. 5 utworzyć dwie oddzielne listy uporządkowane z dwóch serii danych,
a następnie napisać funkcję, która dokona połączenia dwóch list w jedną listę uporządkowaną i
zwróci wskaźnik na tę połączoną listę. Uwaga: “dobre” rozwiązanie polega na przenoszeniu całych
serii elementów list gdy to tylko możliwe.
*/
void LabLZad7()
{
	List l1 = 0, l2 = 0, lMerged = 0;
	List ll[2] = { l1, l2 };
	int i = 2;

	puts("Podaj wartości i wybierz ENTER");
	while(i--)
	{	printf("Lista %d\n", i+1);
		List_Scanf_ToEnd(&(ll[i]));
	}

	//TODO: chyba trzeba poprawić ta funkcję
	lMerged = List_MergeSorted(l1, l2);
	List_Free(lMerged);
}
/*
8. Podobnie jak w zadaniu 7, lecz tym razem procedura ma porównać obie listy. Powinna ona zwrócić
dwie listy elementów, które mają składać się odpowiednio z elementów pierwszej listy nie-należących
do drugiej i odwrotnie. W przypadku, gdy listy są równe, wówczas listy wynikowe będąpuste.
*/
void LabLZad8()
{
	List l1 = 0, l2 = 0, l1subl2, l2subl1;
	puts("Wprowadz liste l1: ");
	List_Scanf_ToEnd(&l1); List_Print(l1);
	puts("\nWprowadz liste l2: ");
	List_Scanf_ToEnd(&l2); List_Print(l2);
	
	l1subl2 = Lists_Subtract(l1, l2);
	puts("\nElementy bedace w l1, niebedace w l2: ");List_Print(l1subl2);
	l2subl1 = Lists_Subtract(l2, l1);
	puts("\nElementy bedace w l2, niebedace w l1: ");List_Print(l2subl1);

	List_Free(l1); List_Free(l2); List_Free(l1subl2); List_Free(l2subl1);
}
/*
9. Napisać procedury/funkcje dołączające, lokalizujące i usuwające elementy danych na
liście a) jednokierunkowej cyklicznej, b) dwukierunkowej, c) dwukierunkowej cyklicznej.
Przetestować kontrolnym wydrukiem działanie programu dla różnych ewentualności, także gdy lista jest pusta.
*/
#define LLZ9_A
void LabLZad9Info()
{
	puts("Opcje dla zadania 9:");
	printf("%s <int> \t- dodanie wartosci do list\n", CmdList[CMD_ADD]);
	printf("%s <int> \t- wyszukanie wartosci w listach\n", CmdList[CMD_FIND]);
	printf("%s <int> \t- usuniecie wartosci z list\n", CmdList[CMD_REMOVE]);
}
void LabLZad9()
{
	ListC lc = 0;
	ListBi lb = 0;
	ListBiC lbc = 0;
	ListElem *lResult = 0; ListBiElem *lbResult = 0;
	int value = 0; char option = 0;

	while( option != CMD_EXIT )
	{
		option = Cmd_Read(0);
		do{
			switch(option)
			{
			case CMD_ADD: scanf(" %d", &value);
				ListC_Add(&lc, value);
				ListBi_Add(&lb, value);
				ListBiC_Add(&lbc, value);
				break;
			case CMD_FIND: scanf(" %d", &value);
				lResult = ListC_Search(lc, value);printf("Wynik dla ListC: %d\n", lResult ? lResult->value : -1);
				lbResult = ListBi_Search(lb, value);printf("Wynik dla ListBi: %d\n", lbResult ? lbResult->value : -1);
				lbResult = ListBiC_Search(lbc, value);printf("Wynik dla ListBiC: %d\n", lbResult ? lbResult->value : -1);
				break;
			case CMD_REMOVE: scanf(" %d", &value);
				ListC_Delete(&lc, value);
				ListBi_Delete(&lb, value);
				ListBiC_Delete(&lbc, value);
				break;
			case CMD_LIST:
				printf("ListC:\t"); ListC_Print(lc);	puts("");
				printf("ListBi:\t"); ListBi_Print(lb);	puts("");
				printf("ListBiC:"); ListBiC_Print(lbc);	puts("");
			case CMD_HELP:
				LabLZad9Info(); break;
			case CMD_EXIT:
				printf("Wyjscie..."); break;
			default:
				puts("Nieprawidlowa opcja!"); break;
			}
		} while( !Cmd_IsEnter() );
	}
	ListC_Free(lc); ListBi_Free(lb); ListBiC_Free(&lbc);
}
/*
10. Zaimplementuj kolejkę priorytetową. Potrzebne będą funkcje: dodające element; usuwające element o największej wartości PRIORYTETU;
funkcja umożliwiająca zmianę wartości PRIORYTETU elementu.
*/
void LabLZad10Info()
{
	puts("Opcje dla zadania 10:");
	printf("%s <int>,<int> \t- dodanie wartosci,priorytetu do kolejki\n", CmdList[CMD_ADD]);
	printf("%s <int>,<int>,<int> \t- zmiana priorytetu na nowy\n", CmdList[CMD_EDIT]);
	printf("%s \t- pobranie elementu o najwiekszym priorytecie\n", CmdList[CMD_GET]);
}
void LabLZad10()
{
	int value, priority, newPriority; char option = 0;
	QueueP qp = 0;
	QueuePElem *qe = 0;
	while( option != CMD_EXIT )
	{
		option = Cmd_Read(0);
		do
		{
			switch (option)
			{
			case CMD_ADD:
				scanf(" %d,%d", &value, &priority);
				QueueP_Push(&qp, value, priority);
				break;
			case CMD_EDIT:
				scanf(" %d,%d,%d", &value, &priority, &newPriority);
				QueueP_SetPriority(&qp, value, priority, newPriority);
				break;
			case CMD_GET:
				qe = QueueP_Pop(&qp); printf("[%d,%d]", qe ? qe->value : -1, qe ? qe->priority : -1);
				break;
			case CMD_HELP:
				LabLZad10Info(); break;
			default:
				puts("Opcja nieprawidlowa");
				continue; break;
			}
		} while ( !Cmd_IsEnter() );
		printf("KOLEJKA: ");QueueP_Print(qp); printf("\n");
	}
	//TODO: dodać zwalnianie pamięci kolejki
}
/*
11. Napisz funkcję, która dla dwóch list jednokierunkowych cyklicznych sprawdzi czy składają się z
takich samych ciągów wartości (nie tylko takich samych zbiorów). Pamiętaj, że wskaźniki mogą
początkowo pokazywać dowolne miejsca na listach.
*/
void LabLZad11()
{
	ListC lc1 = 0, lc2 = 0;
	int areEqual = 0;

	puts("Podaj wartosci listy 1:");
	ListC_Scanf(&lc1);
	puts("Podaj wartosci listy 2:");
	ListC_Scanf(&lc2);

	areEqual = ListsC_areSeriesEqual(lc1, lc2);
	if (areEqual == 1)
		puts("Listy cykliczne zawieraja te same ciagi wartosci");
	else
		puts("Listy cykliczne nie sa tymi samymi ciagami");
}
/*
12. Zaimplementuj operacje na drzewie binarnym z porządkiem: dodawanie, usuwanie, szukanie, minimum,
maksimum, poprzednik i następnik (można korzystać ze wskaźnika na węzeł rodzica).
Uzupełnij program o procedurę kontrolnego drukowania zawartości utworzonego drzewa uwzględnieniem
jego struktury, np:
4
	2
5
	7
		8
	3
			10
		9
	6
*/
void LabLZad12Info()
{
puts("Opcje dla zadania 12:");
printf("%s <int> \t - add value to tree\n", CmdList[CMD_ADD]);
printf("%s <int> \t - search value in tree\n", CmdList[CMD_FIND]);
printf("%s <int> \t - delete value from tree\n", CmdList[CMD_REMOVE]);
printf("%s -n <int> \t - successor of value\n", CmdList[CMD_GET]);
printf("%s -p <int> \t - predecessor of value\n", CmdList[CMD_GET]);
printf("%s -m \t\t - minimum value in tree\n", CmdList[CMD_GET]);
printf("%s -x \t\t - maximum value in tree\n", CmdList[CMD_GET]);
printf("%s -s \t\t - sum of values in tree\n", CmdList[CMD_GET]);
puts("Opcje dla zadania 13:");
printf("%s <name> \t - load tree\n", CmdList[CMD_LOAD]);
printf("%s <name> \t - save tree\n", CmdList[CMD_SAVE]);
}
void LabLZad12()
{
	Tree t = 0, tResult = 0;
	int value = 0, result = 0;
	char option = 0, filename[32] = {0}, c;

	while( option != CMD_EXIT )
	{
		fflush(stdin);
		option = Cmd_Read(0);
		do
		{
			switch(option)
			{
			case CMD_ADD:	scanf(" %d", &value);Tree_Add(&t, value);break;
			case CMD_FIND:	scanf(" %d", &value);tResult = Tree_Search(t, value);
							printf("search result: %d\n", tResult ? tResult->value : -1);break;
			case CMD_REMOVE:	scanf(" %d", &value);Tree_Delete(&t, value);break;
			case CMD_LIST:	Tree_PrintStruct(t); puts(""); break;
			case CMD_GET:
				switch(c = Cmd_GetOpt())
				{
				case 'n':	scanf(" %d", &value);tResult = Tree_Successor(t, Tree_Search(t, value));
							printf("nastepnik : %d\n", tResult ? tResult->value : -1);  break;
				case 'p':	scanf(" %d", &value);tResult = Tree_Predecessor(t,Tree_Search(t, value));
							printf("poprzednik : %d\n", tResult ? tResult->value : -1); break;
				case 'm':	result = Tree_Min(t);
							printf("minimum: %d\n", result);	break;
				case 'x':	result = Tree_Max(t);
							printf("maximum: %d\n", result);	break;
				case 's':	result = Tree_Sum(t); printf("sum: %d", result);break;
				default:	printf("Nie podano przelacznika\n");break;
				}
				break;
			// zadanie 13
			case CMD_LOAD:	scanf(" %s", filename);
						if ((tResult = Tree_Load(filename)) != 0) t = tResult ;break;
			case CMD_SAVE:	scanf(" %s", filename); Tree_Save(t, filename);break;
			// koniec zadania 13
			case CMD_EXIT:	printf("Wyjscie...");break;
			case CMD_HELP:	LabLZad12Info(); break;
			default:
				printf("Nieprawidlowa opcja\n");break;
			}
		} while( !Cmd_IsEnter() );
	}
	Tree_Free(t);
}
/*
13. Uzupełnij poprzednie zadanie o procedurę zapisu utworzonego drzewa z porządkiem do pliku,
tak aby jego późniejsze odczytanie odtworzyło automatycznie strukturę drzewa bez konieczności
ponownego porównywania wczytywanych elementów.
*/
void LabLZad13()
{
// zaimplementowane z zadaniu 12
}
/*	14. Zaimplementuj funkcję, która sprawdzi czy dwa drzewa binarne z porządkiem składają się z
dokładnie takich samych wartości (z takich samych ciągów liczb). Postaraj się aby funkcja działała
jak najszybciej.
*/
void LabLZad14()
{
	Tree t1 = 0, t2 = 0;
	int areEqual = 0;

	puts("Podaj wartosci drzewa 1:");
	Tree_Scanf(&t1);
	Tree_PrintStruct(t1);
	puts("Podaj wartosci drzewa 2:");
	Tree_Scanf(&t2);
	Tree_PrintStruct(t2);

	areEqual = Tree_AreEqualValues(t1, t2);
	if (areEqual == 1)
		puts("Drzewa zawieraja te same ciagi wartosci");
	else
		puts("Drzewa nie sa takie same");
}
/*	15. Stwórz funkcje do obsługi drzewa przypominającego działanie katalogów w systemach operacyjnych. 
Węzły–katalogi przyjmują nazwy (łańcuchy znakowe). W węźle można mieć inne węzły
 *	(katalogi, pliki), tak więc potrzebne jest: dodawanie, usuwanie, szukanie, wyświetlanie zawartości
 *	węzła, . . . 
 */
void LabLZad15Info()
{
	printf("%s \t - pomoc \n", CmdList[CMD_HELP]);
	printf("%s \t - pokaz zawartosc \n", CmdList[CMD_LIST]);
	printf("%s <name> \t - dodaj katalog\n", CmdList[CMD_MKDIR]);
	printf("%s <name> \t - dodaj plik\n", CmdList[CMD_TOUCH]);
	printf("%s <name> \t - usun element o danej nazwie\n", CmdList[CMD_REMOVE]);
}
void LabLZad15()
{
	FSDir root = FSDir_New("root", 0, 0, 0, 0), curr = root;
	FSNode *node;
	char str[32] = {0}, cmd = {0};
	LabLZad15Info();
	while( cmd != CMD_EXIT)
	{
		fflush(stdin);
		FSDir_PrintPath(curr);printf(">");
		cmd = Cmd_Read(0);
		do {
			switch(cmd)
			{
			case CMD_MKDIR: scanf(" %s",str); FSDir_AddFolder(curr, str);	break;
			case CMD_TOUCH: scanf(" %s",str); FSDir_EmptyFile(curr, str);	break;
			case CMD_REMOVE:scanf(" %s",str); FSList_RemoveNode(curr, str,0);	break;
			case CMD_CD:	scanf(" %s", str);FS_GoToDir(&curr, str);		break;
			case CMD_LIST:	FSDir_List(curr, FS_ALL, 1);					break;
			case CMD_HELP:	LabLZad15Info();								break;
			case CMD_EXIT:	printf("Wyjscie...");							break;
			case CMD_FIND:	scanf(" %s",str);
				if( (node = FS_Search(curr, str)) != 0 )
					printf("Znaleziono %s\n", node->name );
				else
					printf("Nie znaleziono\n");
				break;
			case CMD_ERROR:
			default:
				printf("Nieprawidlowa opcja\n");	break;
			}
		} while( !Cmd_IsEnter() );
	}
}
/*	16. 3-drzewo to drzewo, które ma przechowywać punkty na odcinku [0, 1). Każdy węzeł drzewa może
 *	mieć 3 poddrzewa. Każdy węzeł przechowuje jedną wartość, która musi odpowiadać przedziałowiwęzła. 
 *	Węzeł korzeń odpowiada za cały przedział [0, 1), więc może przechowywać dowolną jedną
 *	wartość z tego przedziału. Gdy do drzewa zechcemy włożyć drugą wartość, trzeba będzie dodać
 *	odpowiedni nowy węzeł pod węzłem korzenia. Każdy podwęzeł odpowiada za 1/3 przedziału
 *	rodzica, tak więc dzieci korzenia odpowiadają kolejno za przedziały: [0, 1/3), [1/3, 2/3), [2/3, 1)
 *	(podobnie dla wnuków, etc.). Załóżmy, że do drzewa dodajemy wartości: 0.7, 0.2, 0.25. Wtedy
 *	0.7 znajduje się w korzeniu. 0.2 w lewym podwęźle korzenia a 0.25 w prawym podwęźle dziecka
 *	korzenia (por. rys. poniżej, po lewej). Natomiast, gdy do drzewa włożymy wartości 0.7, 0.25, 0.2,
 *	to powstanie nieco inne drzewo (por. rys. poniżej, drzewo środkowe). Kolejny przykład: 0.2, .5, 0.1,
 *	0.9, 0.95, 0.93 (drzewo po prawej stronie poniższego rysunku). Zaimplementuj funkcję dodającą
 *	wartości do drzewa i funkcję szukającą wartość w drzewie. */
void LabLZad16Info()
{
puts("\n== ZAD 16. options ==\n\
-a <value> \t - add float value to tree\n\
-s <value> \t - search float value in tree\n\
-d <value> \t - delete float value from tree\n");
LabLGlobInfo();puts("");
}
void LabLZad16()
{
	Tree3 t = 0, tResult = 0;
	int value = 0, isErr = 0;
	char option = 0;

	do
	{	isErr = 0; fflush(stdin);
		printf("-opt [value]: ");scanf(" -%c", &option );
		switch(option)
		{
		case 'a':	scanf(" %d", &value);Tree3_Add(&t, value);break;
		case 's':	scanf(" %d", &value);tResult = Tree3_Search(t, value);
					printf("search result: %f\n", tResult != 0 ? tResult->value : -1);break;
		case 'd':	scanf(" %d", &value);Tree3_Delete(&t, value);break;
		case 'e':	printf("wyjscie..."); value=-1;isErr=1;break;
		case 'h':	LabLZad16Info(); break;
		default:
			printf("Nieprawidlowa opcja -%c %d\n", option, value); isErr=1; break;
		}
		if(isErr == 0)
		{ printf("Tree:\n"); Tree3_Print(t); puts(""); }
	} while(value >= 0);
	Tree3_Free(t);
}
/*
17. Zaimplementować z pomocą stosu kalkulator wyznaczający wartość wyrażeń zapisanych w odwrotnej notacji polskiej (ONP). Zakładamy, że wyrażenia mogą składać się z liczb całkowitych,
operatorów +, −, ∗, /, a także operacji neg (minus jednoargumentowy).
Przykłady wyrażeń:
3 4 + 7 *
4 neg 5 2 + neg *
*/
void LabLZad17()
{
	char input[256] = { '\0' };
	int out = 0;
	Stack s = 0;
	puts("Wprowadz wyrazenie ONP do przetworzenia");
	do
	{
		if (!input[0])
			puts("Podaj wyrazenie:");
		scanf(" %s", input);
		out = ONP_ParseString(&s, input);
		printf("| "); Stack_PrintUp(s); printf("< [%d]\n", out);
	} while (input[0] != '=');
}
/*
18. Wczytać wyrażenie ONP i zbudować binarne drzewo tego wyrażenia. Wyznaczyć jego wartość za
pomocą rekursywnej funkcji działajacej na tym drzewie. (składnia wyrażeń jak w zadaniu 17).
*/
void LabLZad18()
{
	char input[256] = { '\0' };
	int out = 0;
	Stack s = 0;
	TreeONP t = 0;
	puts("Wprowadz wyrazenie ONP do przetworzenia");
	do
	{
		if (!input[0])
			puts("Podaj wyrazenie:");
		scanf(" %s", input);
		//out = ONP_ParseString(&s, input);
		out = TreeONP_ParseString(&t, input);
		printf("| ");
		//Stack_PrintRev(s); printf("< [%d]\n", out);
		TreeONP_Print(t); printf("< [%d]\n", out);
	} while (input[0] != '=');
}
/*
19. Porównanie wydajności algorytmów sortowania. Napisać osobne procedury sortowania metodami:
bąbelkową, przez wstawianie, metodą Shella (na bazie sortowania bąbelkowego) oraz quick sort.
W każdej z nich należy stosownie osadzić instrukcje zliczania wykonanych podstawień i porównań
sortowanych danych. Program generuje losowo długą, testową serię liczb (∼ 10 000) i sortuje je
(za każdym razem dane muszą być te same !) kolejnymi metodami, wyświetlając dla każdej z nich
liczbę wykonanych porównań i podstawień.
*/
void LabLZad19()
{

}
/*
20. Zaimplementuj sortowanie przez kopce dla liczb rzeczywistych.
*/
void LabLZad20()
{
	float *fTab = 0;
	int fLen = 0;

	printf("Wprowadz dlugosc ciagu do posortowania: "); scanf(" %d", &fLen);
	printf("Wprowadz ciag liczb rzeczywistych: "); fTab = FloatTab_New_Scanf(fLen);

	FloatTab_HeapSort(fTab, fLen);

	printf("Ciag posortowany rosnaco algorytmem HeapSort: ");
	for( int i = 0; i < fLen; i++)
		printf("%.2f ", fTab[i]);
}
/*
21. Zaimplementuj sortowanie przez zliczanie dla wartości naturalnych od 1 do k. k jest argumentem
funkcji sortującej, podobnie jak dane do posortowania.
*/
void LabLZad21()
{
	uint *uiTab = 0, uiMin = 0, uiMax = 0;
	int uiLen = 0;

	printf("Wprowadz dlugosc ciagu do posortowania: "); scanf(" %d", &uiLen);
	printf("Wprowadz ciag licz naturalnych > 0: "); uiTab = UIntTab_CS_Scanf(uiLen, &uiMin, &uiMax);
	printf("max: %d\n", uiMax);

	UIntTab_CountingSort(uiTab, uiLen, uiMin, uiMax);

	printf("Ciag posortowany rosnaco algorytmem zliczajacym: ");
	for( int i = 0; i < uiLen; i++)
		printf("%u ", uiTab[i]);
}
/*
22. Mając dane grafu nieskierowanego G(V,E) zbadaj jego spójność. Wyświetl liczbę składowych spójności.
Na reprezentację grafu składają się wiersze wejścia: pierwszy zawiera liczbę wierzchołków i
liczbę krawędzi. Zakładamy, że wierzchołki są ponumerowane kolejnymi liczbami całkowitymi od
1. Kolejne wiersze zawierają informację o krawędziach w postaci par wierzchołków.
*/
void LabLZad22()
{

}
/*
23. Zaimplementuj jeden z algorytmów wyznaczania drzewa rozpinającego. Opracuj algorytm w oparciu o poznane struktury danych.
*/
void LabLZad23()
{
	int verts, edges, i;
	GEdge e;
	DJColl djcoll = 0;	// Kolekcja zbiorów rozłączonych
	TreeMS treems = 0;	// Minimalne drzewo rozpinające
	//GEHeap queue = GEHeap_New( edges );	// Kolejka na kopcu
	GEQueue queue = 0;						// Kolejka priorytetowa na liście

	printf("Ile wierzcholkow, krawedzi w k: "); scanf(" %d %d", &verts, &edges);
	puts("Podaj wierzcholekA wierzcholekB waga:");
	for( i = 0; i < edges; i++ )
	{
		printf("%d. va vb w: ", i+1); scanf(" %d %d %d", &(e.va), &(e.vb), &(e.w));
		//GEHeap_Push(H, e);
		GEQueue_Push(&queue, e);
	}
	//GEHeap_Print(queue);
	//GEQueue_Print(queue);

	djcoll = DJColl_New( verts );
	treems = TreeMS_New( verts );

	for( i = 0; i < verts; i++ )
		DJColl_SetCollection(djcoll, i);

	for( i = 1; i < verts; i++ )
	{
		do {
			//e = GEHeap_Head(H);
			//e = GEQueue_Head(H);
			e = *(queue->edge);
			//GEHeap_Pop(H);
			GEQueue_Pop(&queue);
		} while( DJColl_FindVerticeSetId(djcoll, e.va) == DJColl_FindVerticeSetId(djcoll, e.vb));
		TreeMS_AddEdge(treems, e);
		DJColl_UnionCollections(djcoll, e);
	}

	TreeMS_Print(treems);
}

/*
Progi zaliczeniowe (minimalna liczba zadań na daną ocenę):
Ocena Próg (liczba zadań)
3.0 11
3.5 13
4.0 15
4.5 17
5.0 19
Zadania 1, 2, 5, 6, 9, 12, 13, 15, 20, 21, 23 powinny być wykonane przez wszystkich. Szczegóły u
prowadzącego laboratorium.
Uwaga: Zadania 7, 8, 16, 17, 18, 19, 22, 23 są (nieco) czasochłonne.
*/

int LabLMain(int argc, char** argv)
{
#define PROG_NUM 23
	int progNum = 0;
	char cmd = 0;
	Cmd_Init();

	ajlogo_Print();

puts("\
/**\n\
 * Algorytmy i Struktury Danych\n\
 * Lista zadań z laboratorium\n\
 * @date 2014/15r.\n\
 * @author Adrian Jędrzejak\n\
 * @mail 267531@fizyka.umk.pl\n\
 * @authors Norbert Jankowski, Miłosz Michalski\n\
 * @link http://www.is.umk.pl/~norbert/asd/lab-zadania.pdf\n\
 */");
	debug_print("DEBUG MODE");
	printf("πœę©ß←↓→óþąśðæŋ’ə…łżźć„”ńµ≥");

	while( cmd != CMD_EXIT )
	{
		fflush(stdin);
		printf("\n>: ");
		cmd = Cmd_Read(0);
		if (!(progNum >= 0 && progNum <= PROG_NUM)){
			puts("Numer programu poza zakresem");
			continue;
		}
		switch (cmd)
		{
		case CMD_LOAD: scanf("%d", &progNum);
			printf("Program nr %d:\n", progNum);
			LabLista[progNum]();
			ajlogo_Print();break;
		case CMD_HELP: scanf("%d", &progNum);
			printf("Opis programu nr %d", progNum);
			printf(" %s", LabLText[progNum]);			break;
		case CMD_LIST:
			for( int i = 0; i < 24; i++)
				printf("%s\n",LabLList[i]);
														break;
		case CMD_EXIT:
			printf("Wyjscie z programu glownego...");	break;
		default:
			puts("Nieprawidlowa opcja");				break;
		}
	}
	return 0;
}
