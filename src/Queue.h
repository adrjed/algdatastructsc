/**
 * @file ajlogo.h
 * @author Adrian Jędrzejak
 * @brief Struktura kolejki priorytetowej i funkcje
 */

#ifndef AJ_QUEUE_H
#define AJ_QUEUE_H

/**
 * Struktura kolejki priorytetowej
 */
typedef struct _pqueue
{
	struct _pqueue* next;	// następny element kolejki
	int priority;			// priorytet elementu
	int value;				// wartość elementu
} QueuePElem;

/**
 * Typ kolejki priorytetowej
 */
typedef QueuePElem* QueueP;

/**
 * Umieszcza nową wartość w kolejce zgodnie z priorytetem
 * @param qp - kolejka priorytetowa
 * @param value	- wartość do umieszczenia
 * @param priority - priorytet nowej wartości
 */
void QueueP_Push(QueueP *qp, int value, int priority);

/**
 * Umieszcza nowy element kolejki zgodnie z priorytetem
 * @param qp - kolejka priorytetowa
 * @param qpe - element kolejki do umieszczenia
 */
void QueueP_PushElem(QueueP *qp, QueuePElem *qpe);

/**
 * Zdejmuje element o największym priorytecie z kolejki
 * @param qp - kolejką, z której zdejmujemy
 * @return - element o największym priorytecie
 */
QueuePElem* QueueP_Pop(QueueP *qp);

/**
 * Zdejmuje element o określonej wartości i priorytecie
 * @param qp - kolejka, z której zdejmujemy
 * @param value - wartość szukanego elementu
 * @param priority - priorytet szukanej wartości
 * @return - element o szukanej wartości i priorytecie
 */
QueuePElem* QueueP_PopElem(QueueP *qp, int value, int priority);

/**
 * Zmienia priorytet wartości w kolejce na nowy. Element zostaje przemieszczony
 * zgodnie z nowym priorytetem
 * @param qp - kolejka, na której działamy
 * @param value - szukana wartość
 * @param priority - obecny priorytet wartości
 * @param newPriority - nowy priorytet wartości
 */
void QueueP_SetPriority(QueueP *qp,int value, int priority, int newPriority);

/**
 * Wyświetla zawartość kolejki w konsoli
 * @param qp - wyświetlana kolejka
 */
void QueueP_Print(QueueP qp);

#endif//AJ_QUEUE_H
