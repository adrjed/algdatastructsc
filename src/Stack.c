#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

#include "ajextra.h"
#include "Stack.h"

StackElem* StackElem_New(int value, StackElem* prev)
{
	StackElem *se = (StackElem*)malloc(sizeof(StackElem));
	se->value = value;
	se->prev = prev;
	return se;
}

//inline
void StackElem_Free(StackElem* se)
{
	free(se);
}

Stack Stack_New(int height, int data[])
{
	Stack s = 0;
	int i = 0;

	if(height <= 0 || data == 0) return 0;

	while(i < height)
		s = StackElem_New(data[i], s);
	return s;
}

//inline
void Stack_Free(Stack *s)
{
	StackElem *se = *s, *S =*s;

	while(se)
	{
		se = S->prev;
		StackElem_Free(S);
		S = se;
	}
	*s = 0;
}

void Stack_PrintDown(Stack s)
{
	while(s)
	{
		printf(" %d", s->value);
		s = s->prev;
	}
}

void Stack_PrintUp(Stack s)
{
	ifnull(s) return;
	if(s->prev)
		Stack_PrintUp(s->prev);
	printf("%d ", s->value);
}

void Stack_Push(Stack *s, int value)
{
	StackElem *se = StackElem_New(value, *s);
	*s = se;
}

int Stack_Pop(Stack *s)
{
	ifnull(s)
	{	debug_print("Zdejmowanie z pustego stosu");
		return INT_MAX;	}

	int value = (*s)->value;
	StackElem *se = (*s)->prev;

	StackElem_Free(*s);
	*s = se;

	return value;
}

void Stack_PushElem(Stack *s, StackElem* se)
{
	if( !se ) return;

	se->prev = *s;
	*s = se;
}

StackElem* Stack_PopElem(Stack *s)
{
	ifnull(s)
	{	debug_print("Zdejmowanie z pustego stosu");
		return 0;	}

	StackElem *se = *s;
	*s = (*s)->prev;
	return se;
}

/**
 * @warning Funkcja nie przetestowana do końca
 */
Stack Stack_PopMulti(Stack *s, int count)
{
	ifnull(s)
	{	debug_print("Zdejmowanie z pustego stosu");
		return 0;	}
	Stack ns = 0, nshead = *s, S = *s;

	while( count-- )
	{
		ifnull(S)
		{	debug_print("Zbyt dużo zjetych elementow. Stos pusty.");
			break;	}
		ns = S;
		S = S->prev;
	}
	*s = S;
	return nshead;
}
