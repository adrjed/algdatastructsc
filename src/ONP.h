/**
 * @file ONP.h
 * @author Adrian Jędrzejak
 * @brief Struktury i funkcje do działań w Odwrotnej Notacji Polskiej
 */

#ifndef AJ_ONP_H
#define AJ_ONP_H

#include "Stack.h"

typedef enum {
	VALUE_SYMBOL_TYPE,
	OPERATOR_SYMBOL_TYPE,
	FUNCTION_SYMBOL_TYPE
} OnpType;

typedef union _onpSymbolValue
{
	int 	nrValue;	// wartość liczbowa
	char	opValue;	// symbol działania
} OnpValue;

typedef struct _onpSymbol
{
	OnpType type;
	OnpValue value;
} OnpSymbol;

typedef struct _treeonp
{
	OnpType	type;
	OnpValue	v;
	struct _treeonp	*left;
	struct _treeonp *right;
} TreeONPNode;

typedef TreeONPNode* TreeONP;

int TreeONP_ParseString(TreeONP *t,char* input);
void TreeONP_Print(TreeONP t);


OnpSymbol* ONP_ParseSymbol(Stack s, char *in);
/* Parsuje ciąg znaków zapisany w odwrotnej notacji polskiej na wynik */
int ONP_ParseString(Stack *s, char *string);

char* ONP_ConvertToOnp(Stack *s, char* in, char* out);

#endif /* ONP_H_ */
