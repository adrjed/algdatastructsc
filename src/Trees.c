
#include "Trees.h"
#include <limits.h>
#include <stdio.h>
#include <malloc.h>


/*=============================================================TREE=ELEMENT=*/

TreeNode* TreeNode_New(int value, TreeNode *left, TreeNode *right)
{
	TreeNode *tn = (TreeNode*)malloc(sizeof(TreeNode));
	tn->value = value;
	tn->left = left;
	tn->right = right;
	return tn;
}
void TreeNode_Delete(TreeNode *tn)
{

}

/*=====================================================================TREE=*/
/* Rozmieszczenie elmentów - mniejsze na prawo, większe na lewo */


Tree Tree_New(int elemCount, int elemData[])
{
	if (elemCount == 0) return 0;

	Tree t = TreeNode_New(elemData[0], 0, 0);
	for (int i = 1; i < elemCount; i++)
		Tree_Add(&t, elemData[i]);

	return t;
}
void Tree_Free(Tree t)
{
	if( t == 0) return;
	Tree_Free(t->left);
	Tree_Free(t->right);
	free(t); t = 0;
}
void Tree_Delete(Tree *t,int value)
{
	Tree *toDel, del;
	if( *t == 0 ) return;
	t = Tree_Search(t, value);
	if( (*t)->left == 0 || (*t)->right == 0 )
		toDel = t;
	else
	{
		toDel = Tree_Max(&(*t)->left);
		(*t)->value = (*toDel)->value;
	}
	del = *toDel;
	if( (*toDel)->left == 0 )
		*toDel = (*toDel)->right;
	else
		*toDel = (*toDel)->left;
	free(del);
}
void Tree_Add(Tree *t, int value)
{
	TreeNode *tn = (TreeNode*)*t;
	if( *t == 0 )
	{ *t = TreeNode_New(value, 0, 0); return; }

	while(1)
	{
		if(value < tn->value)
		{
			if(tn->left == 0)
			{ tn->left = TreeNode_New(value, 0,0); break; }
			else
			{ tn = tn->left; continue; }
		}
		else if(value > tn->value)
		{
			if(tn->right == 0)
			{ tn->right = TreeNode_New(value, 0,0); break; }
			else
			{ tn = tn->right; continue; }
		}
		else break;
	}
}
void Tree_Print(Tree t)
{
	if(t == 0) return;
	if(t->left)
		Tree_Print(t->left);
	printf(" %d", t->value);
	if(t->right)
		Tree_Print(t->right);
}
void Tree_PrintStruct0(Tree t, int depth, char c)
{
	if(t == 0) return;
	Tree_PrintStruct0(t->left, depth+1, '/');
	for(int i = 0; i < depth; i++)
		putchar(' ');
	printf ("%c%d\n",c, t->value);
	Tree_PrintStruct0(t->right, depth+1, '\\');
}
void Tree_PrintStruct(Tree t)
{
	Tree_PrintStruct0(t, 0, '-');
	putchar('\n');
}
TreeNode* Tree_Search(Tree t,int value)
{
	if( t == 0 ) return 0;
	while( t )
	{
		if( t->value == value )
			return t;
		t = value < t->value ? t->left : t->right;
	}
	return 0;
}
TreeNode* Tree_Parent(Tree t, TreeNode *tChild)
{
	if( t == 0 || t == tChild ) return 0;
	TreeNode* tParent = t;
	while( tParent->left != tChild && tParent->right != tChild )
	{
		if( tChild->value > tParent->value )
			tParent = tParent->right;
		else
			tParent = tParent->left;
	}
	return tParent;
}
Tree Tree_Successor(Tree t, TreeNode *tElem)
{
	TreeNode* nTmp = 0;
	if( tElem->right )
		return Tree_MinNode(tElem->right);
	nTmp = Tree_Parent(t, tElem );
	while( nTmp != 0 && nTmp->left != tElem )
	{
		tElem = nTmp;
		nTmp = Tree_Parent(t, nTmp);
	}
	return nTmp;
}
Tree Tree_Predecessor(Tree t, TreeNode *tElem)
{
	TreeNode* nTmp = 0;
	if( tElem->left )
		return Tree_MaxNode(tElem->left);
	nTmp = Tree_Parent(t, tElem);
	while( nTmp != 0 && nTmp->right != tElem )
	{
		tElem = nTmp;
		nTmp = Tree_Parent(t, nTmp);
	}
	return nTmp;
}
int Tree_Min(Tree t)
{
	if(t == 0) return INT_MAX;
	while( t->left )
		t = t->left;
	return t->value;
}
Tree Tree_MinNode(Tree t)
{
	if(t == 0) return 0;
	while( t->left )
		t = t->left;
	return t;
}
int Tree_Max(Tree t)
{
	if(t == 0) return INT_MIN;
	while( t->right )
		t = t->right;
	return t->value;
}
Tree Tree_MaxNode(Tree t)
{
	if(t == 0) return 0;
	while( t->right )
		t = t->right;
	return t;
}
int Tree_Sum(Tree t)
{
	if( t == 0 ) return 0;
	int sum = 0;
	sum += Tree_Sum(t->left);
	sum += Tree_Sum(t->right);
	return t->value + sum;
}
Tree Tree_Load(char* filename)
{
	Tree t = 0, tHead = 0;
	FILE *file = 0;
	char c = 0;
	int v = 0, isEnd = 0;

	if ((file = fopen(filename, "r")) == 0)
		{ printf("Blad otwarcia pliku. przerywam..."); return 0; }
	while( (c = fgetc(file)) != EOF)
	{
		switch(c)
		{
		case 'H': fscanf(file, " %d", &v);t = tHead = TreeNode_New(v, 0, 0);break;
		case 'L': fscanf(file, " %d", &v);t =  t->left = TreeNode_New(v, 0, 0); break;
		case 'R': fscanf(file, " %d", &v);t =  t->right = TreeNode_New(v, 0, 0);break;
		case 'P': t = Tree_Parent(tHead, t ); break;
		case 'E': isEnd = 1; break;
		}
	}
	fclose(file);
	if( isEnd == 0 )
		printf("Brak konca drzewa. Drzewo moze byc niepelne");
	else
		printf("Wczytano drzewo z pliku %s", filename);
	return tHead;
}
void Tree_Save0(Tree thead, Tree t, FILE *f)
{
	if (t == 0) return;
	if (t == thead) fprintf(f, "H %d ", t->value);
	if (t->left)
	{
		fprintf(f, "L %d ", t->left->value);
		Tree_Save0(thead, t->left, f);
	}
	if (t->right)
	{
		fprintf(f, "R %d ", t->right->value);
		Tree_Save0(thead, t->right, f);
	}
	if (t != thead)
	{
		fprintf(f, "P"); return;
	}
	else
	{
		fprintf(f, " E"); return;
	}
}
void Tree_Save(Tree t, char* filename)
{
	FILE* file = 0;

	if ((file = fopen(filename, "w")) == 0)
	{
		printf("Blad otwarcia pliku. przerywam..."); return;
	}
	Tree_Save0(t, t, file);
	fclose(file);
}
int Tree_AreEqualValues(Tree lt, Tree rt)
{
	TreeNode* l = Tree_MinNode(lt);
	TreeNode* r = Tree_MinNode(rt);

	while (l != 0 && r != 0)
	{
		l = Tree_Successor(lt, l);
		r = Tree_Successor(rt, r);
		if (l == 0 || r == 0)
		{
			if (l == 0 && r == 0)
				return 1;
			else
				return 0;
		}
		if (l->value == r->value)
			continue;
		else
			break;
	}
	return 0;
}
void Tree_Scanf(Tree *t)
{
	int num;
	do {
		scanf(" %d", &num);
			Tree_Add(t, num);
	} while ( !Cmd_IsEnter() );
}

/*============================================================TREE=ITERATOR=*/

TreeIterator Tree_GetIterator(Tree t)
{
	return 0;
}
int Tree_GetValue(TreeIterator ti)
{
	return 0;
}
int Tree_Next(TreeIterator ti)
{
	return 0;
}
void Tree_Reset(TreeIterator ti)
{

}
void Tree_ForEach(TreeIterator ti, void (*FuncOnElem)(int))
{

}


/*===================================================================TREE=3=*/

Tree3 Tree3_New(int elemCount, int elemData[])
{
	return 0;
}
void Tree3_Free(Tree3 t)
{

}
void Tree3_Delete(Tree3 *t,int value)
{

}
void Tree3_Add(Tree3 *t, int value)
{

}
void Tree3_Print(Tree3 t)
{

}
Tree3Node* Tree3_Search(Tree3 t,int value)
{
	return 0;
}

