/* Funkcje testujące poszczególne funkcje operujące na strukturach list */

#ifndef LISTS_TESTS_C
#define LISTS_TESTS_C

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "Tables.h"
#include "Lists.h"

#include "diag.h"

/* Tests environment params */
enum ListTestsEnvironment{
	LIST_LENGTH = 100,
};

/* List Iterator test */
void Func_ForEach(int i)
{
	printf("Kolejny element: %d\n", i);
}

void Test_List_ForEach()
{
	List l = List_New(LIST_LENGTH, IntTab_New_Asc(LIST_LENGTH, 1));
	ListIterator li = List_GetIterator(l);

	for (int i = 0; i < 10; i++)
	{
		DIAG_CLOCK_START
		List_ForEach(li, Func_ForEach);
		DIAG_CLOCK_STOP
	}

	free(li);
	List_Free(l);
}

#endif//LISTS_TESTS_C
