/**
 * @file ajlogo.h
 * @author Adrian Jędrzejak
 * @brief Logo ascii
 */

#ifndef AJ_LOGO_H
#define AJ_LOGO_H

int ajlogo_h = 39;
int ajlogo_w = 50;

char *ajlogo_ascii[39] = {
"                         -.                       ",
"                       .dMs                       ",
"                      +MMM-                       ",
"                    `yMhMm                        ",
"                   .mm:-Mo                        ",
"                  /Ny` oM-                        ",
"                 sM+   hN                         ",
"               `dm-    Ny                         ",
"              -mh`    `Mo                         ",
"             /Ms      -M/                         ",
"            oM/       /M-                         ",
"           yN:        +M-    `.-://+ossyyyyso+-   ",
"         `dm.    .-/+odMdhhdhhyyyysssssoossydMMN/ ",
"        .mNoosyyyo+/-.yM`                 :: .MMm ",
"    -+ohMd/-`         yM-  yN-          -dM: -MM: ",
"  oy: /Nm`            yM- /MM+        :hMN: -NN-  ",
" yd .yMM-             yM/-Nsdd.   ./yNMMM- :Nd.   ",
" mMdMMM/              /MmNm`.yNNNMMMNdMM: +Mo ..  ",
" .syyo.                ohs`    `..`  /M/`ym- sMMy ",
"                                    `Ns.ms`   .-  ",
"//=============================\\    yd/m:        ",
"||Algorytmy i Struktury Danych ||   :Nhy`         ",
"||Lista zadań z laboratorium  ///   mN:           ",
"||2014 / 2015            //==//   .dd`            ",
"||Adrian Jędrzejak       ||     `sdM/             ",
"||267531@fizyka.umk.pl   ||    /m+yN              ",
"\\======================//   `hd..Ms              ",
"                           /No  sM-               ",
"                         `hN-  `Mm                ",
"                        :Nd`   oMs                ",
"                       oMy     NM-                ",
"                     `hMy     /MN                 ",
"                    `dMd      mMh                 ",
"                    yMM:     /MM+                 ",
"                    MMM/    .NMM.                 ",
"                    hMMMs:./mMMd                  ",
"                    `dMMMMMMMMM:                  ",
"                      /hMMMMMm/                   ",
"                         .--.                     "
};


void ajlogo_Print()
{
	for(int i = 0; i < ajlogo_h ; i++)
		printf(" %s\n", ajlogo_ascii[i]);
}

#endif//AJ_LOGO_H
