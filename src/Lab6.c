/* Laboratorium 6 - 3.11.2014 */

/*
 * Lab 3.1 (6) Napisać procedurę i funkcje dołączające, usuwające i
 * 	szukające dla list:
 * 	a) jednocyklicznych
 * 	b) dwukierunkowych
 * 	c) dwukierunkowych cyklicznych
 *
 * Funkcje pomocniczne w tym zadaniu:
 * 	ListC_Add
 * 	ListC_Remove
 * 	ListC_Search
 * 	ListBi_Add
 * 	ListBi_Remove
 * 	ListBi_Search
 * 	ListBiC_Add
 * 	ListBiC_Remove
 * 	ListBiC_Search
 */


/*
 * Lab 3.2 (7) Zaimplementować kolejkę priorytetową:
 * 	a) dodająca
 * 	b) usuwająca (po największej)
 * 	c) zmiana wartości priorytetowych
 */


