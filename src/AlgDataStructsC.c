/* AlgDataStructsC.c : main project file. */

#include <stdio.h>

#include "Laboratoria.h"
#include "Cwiczenia.h"
#include "Testy.h"


int main(int argc, char **argv)
{

	//Lab1Main(argc, argv);
	//Lab2Main(argc, argv);
	//Lab3Main(argc, argv);

	//Cw1Main(argc, argv);
	//Cw2Main(argc, argv);
	//Cw3Main(argc, argv);

	LabLMain(argc, argv);

	//Test_Onp(argc, argv);
	//Test_ConvertToOnp(argc, argv);

	return 0;
}
