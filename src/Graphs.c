#include <stdlib.h>
#include <stdio.h>

#include "Graphs.h"


GEdge* GEdge_New(int w, int va, int vb)
{
	GEdge* ge = malloc(sizeof(GEdge));
	ge->w = w;
	ge->va = va;
	ge->vb = vb;
	return ge;
}

GEHeap GEHeap_New( int length )
{
	GEHeap h = malloc(sizeof(GEHeapElem));
	h->edges = malloc(sizeof(GEdge)*length);
	h->length = length;
	h->position = 0;
	return h;
}

void GEHeap_Delete(GEHeap h)
{
	if( h == 0) return;
	free(h->edges);
	free(h);
}

void GEHeap_Push(GEHeap h, GEdge ge)
{
	int i, j;
	if(h == 0)return;

	i = h->position++;
	j = (i - 1) >> 1;

	while( i && ( h->edges[j].w > ge.w ))
	{
		h->edges[i] = h->edges[j];
		i = j;
		j = ( i - 1) >> 1;
	}
	h->edges[i] = ge;
}

void GEHeap_Pop(GEHeap h)
{
	int i, j;
	GEdge ge;
	if(h == 0) return;

	if( h->position )
	{
		ge = h->edges[--h->position];
		i = 0;
		j = 1;

		while( j < h->position )
		{
			if((j+1 < h->position) && ( h->edges[j+1].w < h->edges[j].w))
				j++;
			if( ge.w <= h->edges[j].w)
				break;
			i = j;
			j = (j << 1) + 1;
		}

		h->edges[i] = ge;
	}
}

void GEHeap_Print(GEHeap h)
{
	GEdge* e;
	if( h == 0 ) return;
	for(int i = 0; i < h->length; i++)
	{
		e = (h->edges)+i;
		printf("%d-%d:%d\n", e->va, e->vb, e->w);
	}
}

GEdge GEHeap_Head(GEHeap h)
{
	return h->edges[0];
}

GEQueue GEQueue_New(GEdge ge, GEQueueElem* next)
{
	GEQueue l = malloc(sizeof(GEQueueElem));
	l->edge = GEdge_New(ge.w, ge.va, ge.vb);
	l->next = next;
	return l;
}
void GEQueue_Push(GEQueue *l, GEdge ge)
{
	GEQueueElem *qn = GEQueue_New(ge, 0), *q = *l, *qprev = 0;
	if( !q )
		*l = qn;
	else
	{
		while(q)
		{
			if(q->edge->w <= qn->edge->w)
			{
				qprev = q;
				q = q->next;
			}
			else
				break;
		}
		if(qprev)
			qprev->next = qn;
		else
			*l = qn;
		qn->next = q;
	}
}
GEQueueElem* GEQueue_Pop(GEQueue *l)
{
	if (!(*l)) return 0;
	GEQueueElem *qe = *l;
	*l = (*l)->next;
	return qe;
}
GEQueueElem* GEQueue_Head(GEQueue l)
{
	return l->edge;
}
void GEQueue_Print(GEQueue q)
{
	while (q)
	{
		printf("%d<->%d :w %d\n", q->edge->va, q->edge->vb, q->edge->w);
		q = q->next;
	}
}

// DEFINICJE DSSTRUCT


DJColl DJColl_New(int length )
{
	DJColl ds = malloc(sizeof(DJCollElem)*length);
	return ds;
}
void DSStruct_Delete(DJColl ds)
{
	free(ds);
}

void DJColl_SetCollection(DJColl dc, int vNr )
{
	dc[vNr].vertNr = vNr;
	dc[vNr].collNr = 0;
}
int DJColl_FindVerticeSetId(DJColl dc, int vNr)
{
	if( dc[vNr].vertNr != vNr)
		dc[vNr].vertNr = DJColl_FindVerticeSetId(dc, dc[vNr].vertNr);
	return dc[vNr].vertNr;
}
void DJColl_UnionCollections(DJColl ds, GEdge e)
{
	int rva, rvb;

	rva = DJColl_FindVerticeSetId(ds, e.va);
	rvb = DJColl_FindVerticeSetId(ds, e.vb);
	if( rva != rvb )
	{
		if( ds[rva].collNr > ds[rvb].collNr )
			ds[rvb].vertNr = rva;
		else
		{
			ds[rva].vertNr = rvb;
			if( ds[rva].collNr == ds[rvb].collNr )
				ds[rvb].collNr++;
		}
	}
}



TreeMSNode* TreeMSNode_New(int v, int w, TreeMSNode* next)
{
	TreeMSNode *t = malloc(sizeof(TreeMSNode));
	t->v = v;
	t->w = w;
	t->next = next;
	return t;
}

TreeMS TreeMS_New( int length )
{
	TreeMS t = malloc(sizeof(TreeMSElem));
	t->nbhArr = malloc(sizeof(TreeMSNode)*length);

	for( int i = 0; i < length; i++)
		t->nbhArr[i] = 0;
	t->length = length-1;
	t->weight = 0;
	return t;
}
void TreeMS_Delete(TreeMS t)
{
	TreeMSNode *p, *r;

	for( int i = 0; i <= t->length; i++)
	{
		p = t->nbhArr[i];
		while( p )
		{
			r = p;
			p = p->next;
			free(r);
		}
	}
	free(t->nbhArr);
	free(t);
}

void TreeMS_AddEdge(TreeMS t,  GEdge e )
{
	TreeMSNode *p;

	t->weight += e.w;
	p = TreeMSNode_New(e.vb, e.w, t->nbhArr[e.va]);
	t->nbhArr[e.va] = p;

	p = TreeMSNode_New(e.va, e.w, t->nbhArr[e.vb]);
	t->nbhArr[e.vb] = p;
}

void TreeMS_Print(TreeMS t)
{
	TreeMSNode *p;
	printf("\n Suma wag krawędzi drzewa = %d \n", t->weight);
	printf("va --|waga|--> vb\n");
	for(int i = 0; i <= t->length; i++)
	{
		//printf("wierzcholek %d: \n", i);
		for( p = t->nbhArr[i]; p; p = p->next )
			printf("%d --|%d|--> %d\n",i, p->w, p->v);
	}
}

//=================================================================

//niekompletne
void Graph_AddFromFile(FILE *f, GEQueue gq)
{
	int w, u, v;
	GEdge e;
	fscanf(f, " %d %d %d", &v, &u, &w);
	do
	{
		fscanf(f, " %d %d %d", &(e.va), &(e.vb), &(e.w));
		if( e.w != 0 )
			GEQueue_Push(&gq, e);
			//GEHeap_Push(gq, e);
		else
			break;

	}while( w == 0 );
}

//niekompletne
void Graph_Scanf(GEQueue gq)
{
	GEdge e;
	printf("Podaj wage, wiezcholek u i v, aby zakonczyc waga == 0\n");
	scanf(" %d %d %d", &(e.w), &(e.va), &(e.vb));
	while ( e.w != 0){
		GEQueue_Push(&gq, e);
		//GEHeap_Push(gq, e);
		scanf(" %d %d %d", &(e.w), &(e.va), &(e.vb));
	}
}


