﻿void LabLZad1();
void LabLZad2();
void LabLZad3();
void LabLZad4();
void LabLZad5();
void LabLZad6();
void LabLZad7();
void LabLZad8();
void LabLZad9();
void LabLZad10();
void LabLZad11();
void LabLZad12();
void LabLZad13();
void LabLZad14();
void LabLZad15();
void LabLZad16();
void LabLZad17();
void LabLZad18();
void LabLZad19();
void LabLZad20();
void LabLZad21();
void LabLZad22();
void LabLZad23();

void (*LabLista[24])() = {
		0,
		LabLZad1,LabLZad2,LabLZad3,LabLZad4,LabLZad5,
		LabLZad6,LabLZad7,LabLZad8,LabLZad9,LabLZad10,
		LabLZad11,LabLZad12,LabLZad13,LabLZad14,LabLZad15,
		LabLZad16,LabLZad17,LabLZad18,LabLZad19,LabLZad20,
		LabLZad21,LabLZad22,LabLZad23
};

char *LabLList[24] = {
	"0. Program testowy",
	"1. Drukowanie list",
	"2. Listy - szukanie, dodawanie, usuwanie",
	"3. Listy - usuwanie elementow parzystych",
	"4. Listy - odwracanie iteracyjne, rekursywne",
	"5. Listy - sortowanie z wartownikiem i bez",
	"6. Listy - usuwanie duplikatow",
	"7. Listy - laczenie z porzadkiem",
	"8. Listy - \"odejmowanie list\"",
	"9. Listy jednokier., dwukier., cykl., dwuk. cykl.",
	"10. Kolejka priorytetowa",
	"11. Sprawdzanie ciagow wartosci w listach",
	"12. Drzewo binarne - operacje",
	"13. Drzweo binarne - czytanie, zapisywanie do pliku",
	"14. Sprawdzanie ciagow wartosi w drzewach",
	"15. Drzewo systemu plikow - operacje",
	"16. 3-drzewo - operacje",
	"17. Kalkulator ONP na stosie",
	"18. Kalkulator ONP na drzewie",
	"19. Porownanie algorytmow sortowania",
	"20. Sortowanie przez kopce",
	"21. Sortowanie przez zliczanie",
	"22. Grafy - badanie spojnosci",
	"23. Grafy - minimalne drzewo rozpinajace"
};

char *LabLText[24] = {
	"0. Testowa tresc zadania\
	Progi zaliczeniowe(minimalna liczba zadan na dana ocene) :\n\
	Ocena Prog(liczba zadan)\n\
	3.0 11\n\
	3.5 13\n\
	4.0 15\n\
	4.5 17\n\
	5.0 19\n\
	Zadania 1, 2, 5, 6, 9, 12, 13, 15, 20, 21, 23 powinny byc wykonane przez wszystkich.Szczegoly u\
	prowadzacego laboratorium.\n\
	Uwaga: Zadania 7, 8, 16, 17, 18, 19, 22, 23 sa(nieco) czasochlonne.",

	"1. Program czyta liczby calkowite podawane z klawiatury az do pojawienia sie wartosci ujemnej.\
	Nalezy utworzyc z nich nieposortowana liste prosta dolaczajac kolejne elementy a) na poczatku,\
	b) na koncu listy.Nastepnie uzupelnic program o procedury drukowania zawartości listy w porzadku\
	prostym i odwrotnym.",

	"2. Uzupelnic program z zad. 1 o funkcje znajdz(n, P) wyszukujaca wartośc n wśrod elementow listy\
	wskazywanej przez P.Funkcja ta powinna zwracac jako wartośc wzkaznik na odnaleziony element\
	listy lub nil gdy go nie znaleziono.Przetestowac ja wczytujac rozne wartości n i czytelnie wyświetlajac wyniki przeszukania.\
	Nastepnie zaimplementowac operacje dodawania nowego elementu m\
	a) przed b) za elementem wskazanym przez funkcje “znajdz”.Zaimplementowac takze operacje\
	c) usuwania wskazanego elementu z listy.Po dodaniu / usunieciu elementu zawartośc listy nalezy kontrolnie wydrukowac.\
	Program powinien dzialac w petli, tak aby mozna bylo wielokrotnien\
	testowac odnajdywanie, dopisywanie i usuwanie elementow, az do calkowitego oproznienia listy.",

	"3. Do programu dodac funkcje usunparz(P) zwracajaca wskaznik na liste powstala z P przez usuniecie\
	wszystkich liczb parzystych.Wydrukowac wynik.",

	"4. Uzupelnic program z zadania 1 o procedure odwracania listy bez generowania nowych elementow\
	(tj.wylacznie przez manipulacje na lacznikach).Zaproponowac oddzielnie rozwiazania iteracyjne\
	i rekursywne.",

	"5. Podobnie jak w zadaniu 1 tworzymy teraz liste posortowana rosnaco. Zrealizowac (i porownac)\
	2 rozwiazania: bez wartownika i z wartownikiem.Uzupelnic program o funkcje wyszukiwania\
	wskazanego elementu korzystajaca z faktu uporzadkowania listy.",

	"6. Usunac z listy a) nieposortowanej, b) posortowanej powtarzajace sie wartości.\
	Np.lista 2 → 3 → 1 → 3 → 4 → 2 ma zostac przetworzona na 2 → 3 → 1 → 4.",

	"7. Korzystajac z procedur zad. 5 utworzyc dwie oddzielne listy uporzadkowane z dwoch serii danych,\
	a nastepnie napisac funkcje, ktora dokona polaczenia dwoch list w jedna liste uporzadkowana i\
	zwroci wskaznik na te polaczona liste.Uwaga: “dobre” rozwiazanie polega na przenoszeniu calych\
	serii elementow list gdy to tylko mozliwe.",

	"8. Podobnie jak w zadaniu 7, lecz tym razem procedura ma porownac obie listy. Powinna ona zwrocic\
	dwie listy elementow, ktore maja skladac sie odpowiednio z elementow pierwszej listy nie - nalezacych\
	do drugiej i odwrotnie.W przypadku, gdy listy sa rowne, wowczas listy wynikowe bedapuste.",

	"9. Napisac procedury/funkcje dolaczajace, lokalizujace i usuwajace elementy danych na\
	liście a) jednokierunkowej cyklicznej, b) dwukierunkowej, c) dwukierunkowej cyklicznej.\
	Przetestowac kontrolnym wydrukiem dzialanie programu dla roznych ewentualności, takze gdy lista jest pusta.",

	"10. Zaimplementuj kolejke priorytetowa. Potrzebne beda funkcje: dodajace element;\
	usuwajace element o najwiekszej wartości PRIORYTETU;\
	funkcja umozliwiajaca zmiane wartości PRIORYTETU elementu.",

	"11. Napisz funkcje, ktora dla dwoch list jednokierunkowych cyklicznych sprawdzi czy skladaja sie z\
	takich samych ciagow wartości(nie tylko takich samych zbiorow).Pamietaj, ze wskazniki moga\
	poczatkowo pokazywac dowolne miejsca na listach.",

	"12. Zaimplementuj operacje na drzewie binarnym z porzadkiem : dodawanie, usuwanie, szukanie, minimum, maksimum, poprzednik i nastepnik(mozna korzystac ze wskaznika na wezel rodzica).\
	Uzupelnij program o procedure kontrolnego drukowania zawartości utworzonego drzewa z uwzglednieniem jego struktury, np :\n\
	4\n\
	2\n\
	5\n\
	7\n\
	8\n\
	3\n\
	10\n\
	9\n\
	6\n",

	"13. Uzupelnij poprzednie zadanie o procedure zapisu utworzonego drzewa z porzadkiem do pliku,\
	tak aby jego pozniejsze odczytanie odtworzylo automatycznie strukture drzewa bez konieczności\
	ponownego porownywania wczytywanych elementow.",

	"14. Zaimplementuj funkcje, ktora sprawdzi czy dwa drzewa binarne z porzadkiem skladaja sie z dokladnie takich samych wartości (z takich samych ciagow liczb). Postaraj sie aby funkcja dzialala\
	jak najszybciej.",

	"15. Stworz funkcje do obslugi drzewa przypominajacego dzialanie katalogow w systemach operacyjnych. Wezly–katalogi przyjmuja nazwy (lancuchy znakowe). W wezle mozna miec inne wezly\
	(katalogi, pliki), tak wiec potrzebne jest : dodawanie, usuwanie, szukanie, wyświetlanie zawartości\
	wezla, . . . ",

	"16. 3-drzewo to drzewo, ktore ma przechowywac punkty na odcinku [0, 1). Kazdy wezel drzewa moze\
	miec 3 poddrzewa.Kazdy wezel przechowuje jedna wartośc, ktora musi odpowiadac przedzialowiwezla.\
	Wezel korzen odpowiada za caly przedzial[0, 1), wiec moze przechowywac dowolna jedna\
	wartośc z tego przedzialu.Gdy do drzewa zechcemy wlozyc druga wartośc, trzeba bedzie dodac\
	odpowiedni nowy wezel pod wezlem korzenia.Kazdy podwezel odpowiada za 1 / 3 przedzialu\
	rodzica, tak wiec dzieci korzenia odpowiadaja kolejno za przedzialy : [0, 1 / 3), [1 / 3, 2 / 3), [2 / 3, 1)\
	(podobnie dla wnukow, etc.).Zalozmy, ze do drzewa dodajemy wartości : 0.7, 0.2, 0.25.Wtedy\
	0.7 znajduje sie w korzeniu. 0.2 w lewym podwezle korzenia a 0.25 w prawym podwezle dziecka\
	korzenia(por.rys.ponizej, po lewej).Natomiast, gdy do drzewa wlozymy wartości 0.7, 0.25, 0.2,\
	to powstanie nieco inne drzewo(por.rys.ponizej, drzewo środkowe).Kolejny przyklad : 0.2, .5, 0.1,\
	0.9, 0.95, 0.93 (drzewo po prawej stronie ponizszego rysunku).Zaimplementuj funkcje dodajaca\
	wartości do drzewa i funkcje szukajaca wartośc w drzewie.",

	"17. Zaimplementowac z pomoca stosu kalkulator wyznaczajacy wartośc wyrazen zapisanych w odwrotnej notacji polskiej(ONP).Zakladamy, ze wyrazenia moga skladac sie z liczb calkowitych,\
	operatorow + , −, ∗, / , a takze operacji neg(minus jednoargumentowy).\
	Przyklady wyrazen :\
	3 4 + 7 *\
		4 neg 5 2 + neg *",

	"18. Wczytac wyrazenie ONP i zbudowac binarne drzewo tego wyrazenia. Wyznaczyc jego wartośc za\
	pomoca rekursywnej funkcji dzialajacej na tym drzewie. (skladnia wyrazen jak w zadaniu 17).",

	"19. Porownanie wydajności algorytmow sortowania. Napisac osobne procedury sortowania metodami:\
	babelkowa, przez wstawianie, metoda Shella(na bazie sortowania babelkowego) oraz quick sort.\
	W kazdej z nich nalezy stosownie osadzic instrukcje zliczania wykonanych podstawien i porownan\
	sortowanych danych.Program generuje losowo dluga, testowa serie liczb(∼ 10 000) i sortuje je\
	(za kazdym razem dane musza byc te same !) kolejnymi metodami, wyświetlajac dla kazdej z nich\
	liczbe wykonanych porownan i podstawien.",

	"20. Zaimplementuj sortowanie przez kopce dla liczb rzeczywistych.",

	"21. Zaimplementuj sortowanie przez zliczanie dla wartości naturalnych od 1 do k. k jest argumentem\
	funkcji sortujacej, podobnie jak dane do posortowania.",

	"22. Majac dane grafu nieskierowanego G(V,E) zbadaj jego spojnośc. Wyświetl liczbe skladowych spojności.\
	Na reprezentacje grafu skladaja sie wiersze wejścia: pierwszy zawiera liczbe wierzcholkow i\
	liczbe krawedzi.Zakladamy, ze wierzcholki sa ponumerowane kolejnymi liczbami calkowitymi od\
	1. Kolejne wiersze zawieraja informacje o krawedziach w postaci par wierzcholkow.",

	"23. Zaimplementuj jeden z algorytmow wyznaczania drzewa rozpinajacego. Opracuj algorytm w oparciu o poznane struktury danych.",

};
