/**
 * @file Tables.h
 * @author Adrian Jędrzejak
 * @brief Funkcje pomocnicze dla tablic różnego rodzaju
 */

#ifndef AJ_TABLES_H
#define AJ_TABLES_H

typedef unsigned int uint;

/**
 *  Generuje tablicę z kolejnymi rosnącymi elementami
 */
int* IntTab_New_Asc(int length, int startValue);

/**
 * Generuje tablice z kolejnymi malejącymi elementami od wartości startValue
 * @param length - długość tablicy wyjściowej
 * @param startValue - wartość początkowa
 * @return - wskaźnik na utworzoną tablicę
 */
int* IntTab_New_Desc(int length, int startValue);

/**
 *  Generuje tablicę z losowymi elementami z przedziału
 * 		[min, max] */
int* IntTab_New_Rand(int length, int min, int max);

/** \brief Pobiera wartości tablicy od użytkownika podaną ilość razy */
int* IntTab_New_Scanf(int length);
float* FloatTab_New_Scanf(int length);

uint* UIntTab_New_Scanf(int length);
uint* UIntTab_CS_Scanf(int length, uint *min,  uint *max);

void UIntTab_CountingSort(uint uitab[], int uilen, uint min, uint max);

/**
 * Sortowanie tablicy za pomocą kopca
 * @param ft - tablica liczb zmiennoprzecinkowych
 * @param fsize - długość tablicy
 */
void FloatTab_HeapSort(float ft[], int fsize);

/**
 * Tablica ciągów znaków
 */
typedef struct _strTab {
	char **string;
	int length;
}StrTab;

void	StrTab_Free(StrTab *s);
StrTab* StrTab_GenTestTab(int length, int strMaxLen);
void	StrTab_Print(int length, char **strTab);
char**	StrTab_Scanf(int count);

#endif//AJ_TABLES_H
