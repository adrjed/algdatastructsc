/**
 * @file ajlogo.h
 * @author Adrian Jędrzejak
 * @brief Struktury różnych rodzajów list i nagłówki do funkcji operujących
 */

#ifndef AJ_LISTS_H
#define AJ_LISTS_H

/**
 * List 	lista jednokierunkowa
 * ListC 	lista jednokierunkowa z "końcem" wskazującym na początek
 * ListBi 	lista dwukierunkowa
 * ListBiC	lista dwukierunkowa z "końcem" wskazującym na początek
 * 			i "początkiem" wskazującym na koniec
 */

/*=============================================================LIST=ELEMENT=*/

/* List element struct type */
typedef struct _le {
	int value;
	struct _le* next;
} ListElem;

ListElem* ListElem_New(int value, ListElem *next);
void ListElem_Delete(ListElem *le);
/**
 * Podmienia sąsiadujące elementy w liście
 * @param prev element poprzedzający elementy do zamiany
 *				jeśli nie ma prev (początek listy) ustaw 0
 * @param swapFrom element który zostanie zmieniony miejscami z swapFrom->next
 */
void ListElem_Swap(ListElem *prev, ListElem *swapFrom);

/*=====================================================================LIST=*/

/* List type */
typedef ListElem* List;

typedef int(*FuncCheck)(int);
typedef int(*FuncCompare)(int, int);

int FuncCheck_IsNegative(int i);
int FuncCheck_IsPositive(int i);

/* Tworzy nową listę o podanej długości.
 * Jeśli data == NULL to wartości elementów będą równe 0.*/
List List_New(int length, int data[]);
/* Zwalnia pamięć całej listy */
void List_Free(List l);
/* Dodaje nowy element na początek listy */
void List_Add(List *l, int value);
int List_AddBefore(List *l, ListElem *le, int value);
int List_AddAfter(List *l, ListElem *le, int value);
/* Dodaje nowy element na koniec listy */
void List_AddToEnd(List *l, int value);
/* Usuwa elementy z wartościami parzystymi */
void List_DeleteEvenNumbers(List *l);
/* Jeśli przekazana funkcja zwroci dla wartości elementu true,
 * to zostanie on usunięty z listy */
void List_RemoveIf(List l, FuncCheck check);
/* Usuń powtarzające się wartości z listy */
void List_RemoveRepeated(List l);
/* Odwraca listę działając na wskaźnikach */
void List_Reverse(List *l);
/* Usuń powtarzające się wartości z listy uporządkowanej */
void List_Sorted_RemoveRepeated(List l);
/* Normalne wyświetlenie elementów listy */
void List_Print(List l);
/* Normalne rekurencyjne wyświetlenie elementów listy */
void List_PrintRec(List l);
/* Odwrotne wyświetlenie elementów listy */
void List_PrintReversed(List l);
/* Odwrotne wyświetlenie elementów listy - rekurencyjnie */
void List_PrintReversedRec(List l);
/* Usuwa wszystkie elementy listy z podana wartością */
int List_RemoveValuesOf(List *l, int value);
/* Sortuje listę. order 0 - malejąco, 1 rosnąco */
void List_Sort(List *l, int order);
/* Połączenie końca l1 do z początkiem l2.
 * Oba wskaźniki wskazują potem na początek l1. */
void List_MergeToEnd(List *l1, List *l2);
/* Skopiowanie wartości l2 od pewnego indeksu l1.
 * l2 zostaje niezmieniona*/
void List_MergeStartAt(List *l1, List l2, int l1StartIndex);
/* Łączy dwie posortowane rosnąco listy w jedną i zwraca wskaźnik. l1 i l2 niezmienione*/
List List_MergeSorted(List l1, List l2);
/* Zwraca wskaźnik na element z szukaną wartością lub 0 jeśli nie znaleziono */
ListElem* List_Search(List l, int value);
/* Zwraca średnią wartość elementów w liście */
int List_Sum(List l);
/* Zwraca ilość elementów o podanej wartośći w liście */
int List_ValueCount(List l, int value);
/* Zwraca długość listy */
int List_Length(List l);
/* Zwraca średnią wartość elementów w liście */
float List_Average(List l);
/* Zwraca elementy nalezace do l1, nienalezace do l2 */
List Lists_Subtract(List l1, List l2);
/* Dodaje wartości do listy dopóki nie wprowadzimy wartości breakVal */
void List_Scanf(List *l);
void List_Scanf_ToEnd(List *l);

/*===============================================================QUEUE=LIST=*/
/**
 * Dodaje elementy do listy rosnąco. Jeśli wartość powtarza się, przerywa
 * @param lh - głowa listy
 * @param value - dodawana wartość
 */
void ListQ_Add(List *lh, int value);

/*============================================================LIST=ITERATOR=*/
/* Struktura iteratora w liście */
typedef struct _lIter
{
	List head;
	ListElem *current;
} ListIteratorStruct;

typedef ListIteratorStruct* ListIterator;

ListIterator List_GetIterator(List l);
int		List_GetValue(ListIterator li);
int		List_Next(ListIterator li);
void	List_Reset(ListIterator li);
void	List_ForEach(ListIterator li, void(*FuncOnElem)(int));

/*==============================================================STRING=LIST=*/

/* String element struct type*/
typedef struct _sle {
	char *string;
	struct _sle* next;
} StrListElem;

/* String list type */
typedef StrListElem* StrList;

void StrList_Add(StrList *sl, char *str);
void StrList_Print(StrList sl);
void StrList_Free(StrList sl);

/*==============================================================CYCLIC=LIST=*/

typedef List ListC;

ListC ListC_New(int length, int data[]);
void ListC_Free(ListC lc);
void ListC_Add(ListC *l ,int value);
void ListC_Delete(ListC *l, int value);
ListElem* ListC_Search(ListC l, int value);
void ListC_Print(ListC l);
void ListC_Scanf(ListC *lc);
int ListC_Length(ListC lc);
int ListsC_areSeriesEqual(ListC lc1,ListC lc2);

/*======================================================BI=DIRECTIONAL=LIST=*/

/* Bi-directional and cyclic list structure */

typedef struct _biLe ListBiElem;
/* Bi-list element struct type */
typedef struct _biLe {
	int value;
	ListBiElem *prev;
	ListBiElem *next;
} ListBiElem;

/* Tworzy nowy element listy dwukierunkowej */
ListBiElem* ListBiElem_New(int value, ListBiElem *prev, ListBiElem *next);
/* Zwalnia pamięć całej listy dwukierunkowej */
void ListBiElem_Delete(ListBiElem *ble);

/*======================================================BI=DIRECTIONAL=LIST=*/

/* Bi-list type */
typedef ListBiElem* ListBi;
/* Bi-list cyclic type */
typedef ListBiElem* ListBiC;

/* Tworzy nową listę dwukierunkową */
ListBi ListBi_New(int length, int data[]);
void ListBi_Free(ListBi lb);
void ListBi_Add(ListBi *lb, int value);
ListBiElem* ListBi_Search(ListBi lb, int value);
void ListBi_Delete(ListBi *lb,int value);
void ListBi_Print(ListBi lb);

/* Tworzy nową cykliczną liste dwukierunkową */
ListBi ListBiC_New(int length, int data[]);
void ListBiC_Free(ListBiC lbc);
void ListBiC_Add(ListBiC *lbc, int value);
ListBiElem* ListBiC_Search(ListBiC lb, int value);
void ListBiC_Delete(ListBi *lb,int value);
void ListBiC_Print(ListBiC lbc);

#endif//LISTS_H
