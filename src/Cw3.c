/* Cwiczenia 3 - 17.10.2014 */
/* Wykona� �wiczenia w wersji iteracyjnej i rekurencyjnej */
#include <stdio.h>
#include <stdlib.h>

#include "Lists.h"
#include "Tables.h"

/*
 * Ćw 3.1. (10) Napisz procedurę sprawdzania podanej liczby na liście
 */
int List_IsNum(List l, int num)
{
	while (l)
	{
		if (l->value == num)
			return 1;

		l = l->next;
	}
	return 0;
}

int List_IsNumRec(List l, int num)
{
	if (l)
	{
		if (l->value == num)
			return 1;
		else
			return List_IsNumRec(l->next, num);
	}
	return 0;
}

void Cw3Zad1()
{
	int* it = IntTab_New_Scanf(5);
	int num = 0;
	List l = List_New(5, it);
	free(it);

	printf("Jakiej liczby szukac: ");
	scanf(" %d", &num);

	if (List_IsNum(l, num))
		printf("IsNum - znaleziono\n");
	else
		printf("IsNum - nie znaleziono\n");

	if (List_IsNumRec(l, num))
		printf("IsNumRec - znaleziono\n");
	else
		printf("IsNumRec - nie znaleziono\n");

	List_Free(l);
}

/*
 * Ćw 3.2. (11) Funkcja, która sprawdza czy lista jest posortowana
 */
int List_IsSorted(List l)
{
	int dir = 0;
	if (!l) return 0;
	if (!l->next) return 1;
	if (!l->next->next) return 1;

	while (1)
	{
		if (!l->next)
			return 0;
		else if (l->value == l->next->value)
			l = l->next;
		else break;
	}
	if (l->value > l->next->value)
	{
		dir = -1;
		while (l->next)
		{
			if (l->value < l->next->value)
				return 0;
			l = l->next;
		}
	}
	else
	{
		dir = 1;
		while (l->next)
		{
			if (l->value > l->next->value)
				return 0;
			l = l->next;
		}
	}
	return dir;
}

int List_IsSortedRec(List l)
{
	return 0;
}

#define TAB_LEN 5

void Cw3Zad2()
{
	int *it = IntTab_New_Scanf(TAB_LEN);
	List l = List_New(TAB_LEN, it);
	int isSorted = List_IsSorted(l);

	switch (isSorted)
	{
	case 1:
		printf("Posortowana rosnaco\n"); break;
	case -1:
		printf("Posortowana malejaco\n"); break;
	case 0:
		printf("Nieposortowana\n"); break;
	default:
		printf("Blad isSorted = %d", isSorted); break;
	}

	free(it);
	List_Free(l);
}

/*
 * Ćw 3.3. (12) Funkcja tworząca dwie listy z jednej:
 * 	- jedna z parzystymi wartościami
 * 	- druga z nieparzystymi wartościami
 */
void Cw3Zad3()
{

}

int Cw3Main(int argc, char **argv)
{
	Cw3Zad1();
	//Cw3Zad2();
	//Cw3Zad3();
	return 0;
}
