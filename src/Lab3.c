/* Laboratorium 3 - 20.10.2014 */

#include "Lists.h"

#include <limits.h>
/*
 * Lab 3.1 (6) Napisać procedurę i funkcje dołączające, usuwające i
 * 	szukające dla list:
 * 	a) jednocyklicznych
 * 	b) dwukierunkowych
 * 	c) dwukierunkowych cyklicznych
 *
 * Funkcje pomocniczne w tym zadaniu:
 * 	ListC_Add
 * 	ListC_Remove
 * 	ListC_Search
 * 	ListBi_Add
 * 	ListBi_Remove
 * 	ListBi_Search
 * 	ListBiC_Add
 * 	ListBiC_Remove
 * 	ListBiC_Search
 */

#define WATCH INT_MAX

// coś będzie źle
void Lab3Zad1a(int count)
{
	ListC l = 0, lHead = 0;
	int i = 0;
	if(!count) return;

	lHead = ListElem_New(i++, 0);
	l = lHead;

	for(; i < count; i++)
		ListC_Add(&lHead, i);
}

void Lab3Zad1b()
{

}

void Lab3Zad1c()
{

}

void Lab3Zad1()
{

}

/*
 * Lab 3.2 (7) Zaimplementować kolejkę priorytetową:
 * 	a) dodająca
 * 	b) usuwająca (po największej)
 * 	c) zmiana wartości priorytetowych
 */


void Lab3Zad2()
{

}

int Lab3Main(int argc, char** argv)
{
	return -1;
}
