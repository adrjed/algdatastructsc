/**
 * @file command.c
 * @author Adrian Jędrzejak
 * @brief Możliwość używania komend wieloznakowych
 */

#include "command.h"
#include <stdio.h>
#include <ctype.h>

//=================================================================SAFE=READ=/

#ifndef _MSC_VER
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
const char* scanf_validate( const char* fmt, const char* file, long line )
{
	const char* p = fmt;
	while (1)
	  {
	  p = strstr( p, "%s" );
	  if (p == NULL) break;
	  if ((p == fmt) || (*(p-1) != '%'))
		{
		fprintf( stderr, "Hey, you used \"%%s\" in %s: line %d!\n", file, line );
		abort();
		}
	  }
	return fmt;
}
#endif

//===============================================================COMMAND=TAB=/

void Cmd_Init()
{
	//CmdList[CMD_ERROR]
	//CmdList[CMD_OK]
	CmdList[CMD_EXIT]	= "exit";
	CmdList[CMD_ADD]	= "add";
	CmdList[CMD_CD]		= "cd";
	CmdList[CMD_COPY]	= "cp";
	CmdList[CMD_CREATE]	= "new";
	CmdList[CMD_EDIT]	= "edit";
	CmdList[CMD_FIND]	= "find";
	CmdList[CMD_GET]	= "get";
	CmdList[CMD_HELP]	= "help";
	CmdList[CMD_LIST]	= "ls";
	CmdList[CMD_LOAD]	= "load";
	CmdList[CMD_MKDIR]	= "mkdir";
	CmdList[CMD_MOVE]	= "mv";
	CmdList[CMD_REMOVE]	= "rm";
	CmdList[CMD_SAVE]	= "save";
	CmdList[CMD_TOUCH]	= "touch";
	//CmdList[CMD_LENGTH]
}

int Cmd_IsEnter()
{
	static char c = 0;

	if( (c = getchar()) == '\n')
		return 1;
	else
		ungetc(c, stdin);
	return 0;
}

char Cmd_GetOpt()
{
	static char a,b,c;
	if( (a = getchar()) == ' ')
	{	if( (b = getchar()) == '-')
		{	if( isalpha(c = getchar()) )
			{	return c;	}
			ungetc(c, stdin);	}
		ungetc(b, stdin);	}
	ungetc(a, stdin);
	return 0;
}

int Cmd_GetID(char* cmd)
{
	for( int i = 1; i < CMD_LENGTH; i++)
		if( strcmp(cmd, CmdList[i]) == 0)
			return i;
	return CMD_ERROR;
}

int Cmd_Read(char* cmd)
{
	char input[8];

	//scanf_s(" %s", input);
	scanf(" %s", input);
	if(cmd)
		cmd = input;
	return Cmd_GetID(input);
}
