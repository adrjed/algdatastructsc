/**
 * @file Graphs.h
 * @author Adrian Jędrzejak
 * @brief Definicja struktur pomagających przy tworzeniu struktury grafu. 
 *  Dostępne różne implementacje w zależności od potrzeb zadania.
 */

#ifndef AJ_GRAPHS_H
#define AJ_GRAPHS_H

/*===============================================================GRAPH=EDGE=*/

/** GraphEdge
 * Struktura krawędzi grafu
 */
typedef struct gedge_s
{
	int w;	// waga krawędzi
	int va;	// wierzchołek źródłowy
	int vb; // wierzchołek końcowy
} GEdge;

/**
 * Alokuje pamięć dla nowej krawędzi grafu
 * @param w - waga krawędzi
 * @param va - numer krawędzi początkowej
 * @param vb - numer krawędzi końcowej
 * @return - wskaźnik na strukturę krawędzi
 */
GEdge* GEdge_New(int w, int va, int vb);

/*=========================================================GRAPH=EDGES=HEAP=*/

/** GraphEdgesHeap Element
 * Struktura zbioru krawędzi grafu za pomocą kopca w tablicy
 */
typedef struct geheap_s
{
	GEdge *edges;	// tablicowa reprezentacja kopca krawędzi
	int length;		// długość tablicy na kopiec
	int position;		// stan zapełnienia kopca
} GEHeapElem;

/**
 * Typ zbioru krawędzi grafu w kopcu
 */
typedef GEHeapElem* GEHeap;

/**
 * Alokuje pamięć dla zbioru krawędzi grafu
 * @param - length ilość krawędzi w grafie
 * @return - strukturę zbioru krawędzi
 */
GEHeap GEHeap_New( int length );
void GEHeap_Delete(GEHeap h);

/**
 * Umieszcza krawędź w zbiorze zgodnie z zasadami kopca
 * @param h - zbiór do którego dodajemy
 * @param ge - dodawana krawędź
 */
void GEHeap_Push(GEHeap h, GEdge ge);

/**
 * Usuwa z kopca czubek i przebudowuje kopiec
 * @param h - zbiór z którego usuwamy
 */
void GEHeap_Pop(GEHeap h);

/**
 * Drukuje informacje o krawędziach w zbiorze
 * @param h
 */
void GEHeap_Print(GEHeap h);

/*===================================GRAPH=EDGES=WEIGHT-PRIORITY=QUEUE=LIST=*/

/** GraphEdges PriorityQueueList
 * Kolejka priorytetowa w postaci listy łączonej krawędzi
 * sortowana od najniższej do najwyższej wagi krawędzi
 */
typedef struct gelist_s
{
	GEdge *edge;			// lista krawędzi zbioru
	struct gelist_s* next;	// następna lista krawędzi
} GEQueueElem;

typedef GEQueueElem* GEQueue;

/**
 * Alokuje nowy element kolejki
 * @param ge - krawędź przechowywana w elemencie kolejki
 * @param next - wskaźnik na kolejny element kolejki
 * @return - wskaźnik na nowy element kolejki
 */
GEQueue GEQueue_New(GEdge ge, GEQueueElem* next);
/**
 * Umieszcza nowy element w kolejce, sortując według wagi krawędzi. Krawędź
 * zostaje obudowana w element kolejki i dopiero w niej umieszczana
 * @param l - kolejka do której dodajemy
 * @param ge - dodawana krawędź
 */
void GEQueue_Push(GEQueue *l, GEdge ge);
/**
 * Zdejmuje głowę z kolejki i zwraca ją
 * @param l - kolejka z której zdejmujemy głowę
 * @return - usunięta z kolejki głowa
 */
GEQueueElem* GEQueue_Pop(GEQueue *l);
/**
 * Zwraca wskaźnik na głowę kolejki, nie modyfikując jej
 * @param l - kolejka z ktorej pobiramy wskaźnik
 * @return - wskaźnik na głowę kolejki
 */
GEQueueElem* GEQueue_Head(GEQueue l);
/**
 * Wyjświetla informacje o krawędziach, posortowane
 * @param q - kolejka z której czytamy dane
 */
void GEQueue_Print(GEQueue q);

/*======================================================DISJOINT=COLLECTION=*/

/** DisjointCollectionElem
 * Element zbioru rozłącznego
 * Przechowuje numer krawędzi oraz numer zbioru do którego należy krawędź
 */
typedef struct dsnode_s
{
	int vertNr;	// numer wierzchołka
	int collNr;	// numer kolekcji
}DJCollElem;

/** DisjointCollection
 * Tablica elementów zbioru rozłącznego
 * wykrywa cykle przy dodawaniu elementów do minimalnego drzewa rozpinającego
 */
typedef DJCollElem* DJColl;

DJColl DJCOll_New(int length);

/**
 * Inicjuje kolekcję dla pewnego wierzchołka
 * @param dc - kolekcja, na której działamy
 * @param vNr - indeks wierzchołka
 */
void DJColl_SetCollection(DJColl dc, int vNr);

/**
 * Zwraca numer zbioru do którego należy dany wierzhchołek
 * @param dc - kolekcja, na ktorej działamy
 * @param vNr - indeks wierzchołka zbioru
 * @return - indeks zbioru danego wierzchołka
 */
int DJColl_FindVerticeSetId(DJColl dc, int vNr);

/**
 * Łączy oddzielne zbiory połączone określoną krawędzią
 * @param dc - kolekcja, na której działamy
 * @param e - wierzchołek, którym połączone są oba ziory
 */
void DJColl_UnionCollections(DJColl dc, GEdge e);

/** TreeMinimalSpanningNode
 * Element minimalnego drzewa rozpinającego
 */
typedef struct treemsnode_s
{
	struct treemsnode_s* next;
	int w;
	int v;
}TreeMSNode;

TreeMSNode* TreeMSNode_New(int v, int w, TreeMSNode* next);

/** TreeMinimalSpanningElem
 * Struktura minimalnego drzewa rozpinającego
 */
typedef struct treems_s
{
	TreeMSNode **nbhArr;	// neighbourhood array - tablica listy sąsiedztwa
	int length;				// liczba komórek w tablicy
	int weight;				// waga całego drzewa
}TreeMSElem;

/** TreeMinimalSpanning
 * Minimalne drzerwo rozpinające
 */
typedef TreeMSElem* TreeMS;

TreeMS TreeMS_New( int n );
void TreeMS_Delete(TreeMS t);
void TreeMS_AddEdge(TreeMS t,  GEdge e );
void TreeMS_Print(TreeMS t);

void Graph_AddFromFile(FILE *f, GEQueue G);
void Graph_Scanf(GEQueue G);

#endif//AJ_GRAPHS_H
