#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "ajextra.h"

int int_RandMM(int min, int max)
{
	static char isInit = 0;
	if (isInit == 0){
		srand((int)time(0));
		isInit = 1;
	}

	int tmp;
	if (max >= min)
		max -= min;
	else
	{
		tmp = min - max;
		min = max;
		max = tmp;
	}
	return max ? (rand() % max + min) : min;
}

void stdin_Clear()
{
	char c;
	while((c = getchar()) != '\n' && c != EOF);
}
