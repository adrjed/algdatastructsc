#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ajextra.h"
#include "command.h"
#include "Lists.h"

/*==========================================================================*/

/*
 * ListElem type functions
 */

ListElem* ListElem_New(int value, ListElem *next)
{
	ListElem *le = 0;

	le = (ListElem*)malloc(sizeof(ListElem));
	le->value = value;
	le->next = next;

	return le;
}
void ListElem_Delete(ListElem *le)
{
	free(le);
	le = 0;
}
/**
 * \brief Podmienia sąsiadujące elementy w liście
 * \param prev element poprzedzający elementy do zamiany
 *				jeśli nie ma prev (początek listy) ustaw 0
 * \param swapFrom element który zostanie zmieniony miejscami z swapFrom->next
 */
void ListElem_Swap(ListElem *prev, ListElem *swapFrom)
{
	ListElem *swapTo = swapFrom->next;
	prev->next = swapTo->next;
}

/*==========================================================================*/

/*
 * List type functions
 */

int FuncCheck_IsNegative(int i)
{
	return 1 < 0 ? 1 : 0;
}
int FuncCheck_IsPositive(int i)
{
	return 1 > 0 ? 1 : 0;
}
int FuncCompare_LT(int a, int b)
{
	return a < b ? 1 : 0;
}
int FuncCompare_GT(int a, int b)
{
	return a > b ? 1 : 0;
}

List List_New(int length, int data[])
{
	List l = 0;
	ListElem *le = 0;

	while (length--)
	{
		le = (ListElem*)malloc(sizeof(ListElem));
		le->value = data[length];
		le->next = l;
		l = le;
	}
	return l;
}
void List_Free(List l)
{
	ListElem *next = 0;
	int length = List_Length(l);

	while (length--)
	{
		next = l->next;
		free(l);
		l = next;
	}
}
void List_Add(List *l, int value)
{
	*l = ListElem_New(value, *l);
}
int List_AddBefore(List *l, ListElem *le, int value)
{
	List li = *l;

	if( !*l ) return 1;
	if( *l == le)
	{
		List_Add(l, value);
		return 0;
	}
	while( li->next != le || !li->next )
		li = li->next;
	li->next = ListElem_New(value, le);
	return 0;
}
int List_AddAfter(List *l, ListElem *le, int value)
{
	ListElem *nle = ListElem_New(value, le->next);
	le->next = nle;
	return 0;
}
void List_AddToEnd(List *l, int value)
{
	List li = *l;
	if (li == 0)
	{
		*l = ListElem_New(value, 0);
		return;
	}
	while(li->next)
		li = li->next;
	li->next = ListElem_New(value, 0);
}
void List_DeleteEvenNumbers(List *lf)
{
	List l = 0, prevl = 0, copy = 0;
	if (!(*lf)) return;
	// jesli ciag początkowych elementów jest parzysty
	while ((*lf)->value % 2 == 0)
	{
		copy = (*lf)->next;
		free(*lf);
		*lf = copy;
		if ((*lf) == 0) return;
	}
	// jeśli wystąpił element nieparzysty
	prevl = l = (*lf);
	l = l->next;
	while (l)
	{
		if (l->value % 2 == 0)
		{
			prevl->next = l->next;
			free(l);
			l = prevl->next;
		}
		else
		{
			prevl = l;
			l = l->next;
		}
	}
}
void List_RemoveIf(List l, FuncCheck check)
{
	List pl = 0, // poprzedni element
		 cl = l; // bieżący element

	while (!pl) // sprawdzanie dla głowy listy
	{
		if (check(l->value))
		{
			cl = cl->next;
			free(l);
			l = cl;
		}
		else
		{
			pl = cl;
			cl = cl->next;
			l = cl;
		}
	}
	while (cl)
	{
		if (check(cl->value))
		{
			pl->next = cl->next;
			free(cl);
			cl = pl->next;
		}
		else
		{
			pl = cl;
			cl = cl->next;
		}
	}
}
void List_RemoveRepeated(List l)
{
	int uniqVal = 0;
	List lHead = l, pl = 0;

	if (!l)return;
	while (l)
	{
		uniqVal = l->value;
		pl = l;
		l = l->next;
		while (l)
		{
			if (l->value == uniqVal)
			{
				pl->next = l->next;
				free(l);
				l = pl->next;
			}
			else
			{
				pl = l;
				l = l->next;
			}
		}
		l = lHead->next;
		lHead = lHead->next;
	}
}
void List_Sorted_RemoveRepeated(List l)
{
	int uniqVal = 0;
	List pl=0, dl=0;
	if (!l) return;
	while(l)
	{
		uniqVal = l->value;
		pl = l; l = l->next;
		while(l)
		{
			if(l->value == uniqVal)
			{
				dl = l;
				l = l->next;
				free(dl);
			}
			else
			{
				pl->next = l;
				break;
			}
		}
	}
	pl->next = l;
}
void List_Reverse(List *l)
{
	List lhead = *l, ltmp = 0;

	if (lhead == 0) return;
	if (lhead->next == 0) return;

	while (lhead->next != 0)
	{
		ltmp = lhead->next;	// ltmp = nowa głowa
		lhead->next = ltmp->next; // podmieniamy następny element starej głowy na następny nowej
		ltmp->next = *l; // podmieniamy next nowej głowy na aktualną głowę
		*l = ltmp; // podmieniamy aktualną głowę na nową głowę
	}
}
void List_Sort(List *l, int order)
{
	FuncCompare funcCmp = 0;
	int listLength = List_Length(*l),
		sortedCount = 0;
	List lCurr = (*l), lTmp = 0, lPrev = 0;

	if(!(*l)) return;
	if(!(*l)->next) return;
	//if(!(*l)->next->next) return;

	if(order == 0)	// malejąco
		funcCmp = FuncCompare_LT;
	else			// rosnąco
		funcCmp = FuncCompare_GT;

	while((listLength-1) != sortedCount)
	{
		sortedCount = 0;
		lCurr = (*l);
		lPrev = 0;
		while(lCurr->next)
		{
			if(funcCmp(lCurr->value, lCurr->next->value))
			{
				if(!lPrev)
				{
					lTmp = lCurr->next;
					lCurr->next = lCurr->next->next;
					lTmp->next = lCurr;
					(*l) = lTmp;
					lCurr = lTmp;
				}
				else
				{
					lPrev->next = lCurr->next;
					lCurr->next = lPrev->next->next;
					lPrev->next->next = lCurr;
					lCurr = lPrev->next;
				}
			}
			else
			{
				sortedCount++;
			}

			lPrev = lCurr;
			lCurr = lCurr->next;
		}
	}
}
void List_MergeToEnd(List *l1, List *l2)
{
	List l = *l1;
	if (!l)
		(*l1) = (*l2);

	while (l->next)
		l = l->next;

	l->next = (*l2);
	(*l2) = (*l1);
}
void List_MergeStartAt(List *l1, List l2, int l1StartIndex)
{
	List l = (*l1);
	while(l1StartIndex-- || !l->next)
		l = l->next;

	if(!(*l1))
	{
		(*l1) = ListElem_New(l2->value, 0);
		l = (*l1);
	}
	while(!l2)
	{
		if(!l->next)
			l->next = ListElem_New(l2->value, 0);
		else
			l->next->value = l2->value;

		l = l->next;
	}
}

/**
 * TODO: niedokończone sortowanie list po połączeniu
 */
List List_MergeSorted(List l1, List l2)
{
	/*if (l1 == 0) return l2;
	if (l2 == 0) return l1;

	List ltmp = 0;
	if (l1->value > l2->value)	// najmniejszy element na głowę
	{
		l1 = ltmp;
		l1 = l2;
		l2 = ltmp;
	}
	List lhead = l1->value < l2->value ? l1 : l2;

	while (l1 != 0 && l2 != 0)
	{
		//while (l1->value < l2->value)
	}*/
	debug_print("Niezaimplementowane");
	return 0;
}
void List_Print(List l)
{
	while (l)
	{
		printf("%d ", l->value);
		l = l->next;
	}
	printf("|");
}
void List_PrintRec(List l)
{
	if(!l)
		printf("|");
	else
	{
		printf("%d ", l->value);
		List_PrintRec(l->next);
	}
}
void List_PrintReversed(List l)
{
	int *intTab = 0,
		intLength = List_Length(l),
		i = 0;

	intTab = (int*)malloc(sizeof(int)*intLength);
	while (l)
	{
		intTab[i++] = l->value;
		l = l->next;
	}

	for (i = intLength - 1; i >= 0; i--)
		printf("%d ", intTab[i]);

	free(intTab);
}
/* Odwrotne wyświetlenie elementów listy - rekurencyjnie */
void List_PrintRR(List l)
{
	if(!l)
		return;
	else
	{
		List_PrintRR(l->next);
		printf("%d ", l->value);
	}
}
void List_PrintReversedRec(List l)
{
	List_PrintRR(l);
	printf("|");
}
/* Usuwa wszystkie elementy listy z podana wartością */
int List_RemoveValuesOf(List *l, int value)
{
	List	tmpl = *l,			// temporary list
			pl = 0,				// previous list
			nl = *l;			// next list
	int isRemoved = 0;

	if(!nl) return isRemoved;
	while( nl )
	{
		if(nl->value == value)	// jeśli wartość głowy listy sie zgadza, zastępujemy głowę nowym elementem
		{	
			tmpl = nl->next;
			free(nl);
			if (!pl) 
				(*l) = tmpl;
			else
				pl->next = tmpl;
			nl = tmpl;
			isRemoved = 1;
		}
		else // jeśli nie to niegłowa do kolejnego sprawdzenia przypisywana jest do el
		{
			pl = nl;
			nl = nl->next;
		}
	}
	return isRemoved;
}
ListElem* List_Search(List l, int value)
{
	while(l)
	{
		if(l->value == value)
			return l;
		else
			l = l->next;
	}
	return 0;
}
int List_Sum(List l)
{
	int sum = 0;

	while (l)
	{
		sum += l->value;
		l = l->next;
	}
	return sum;
}
int List_ValueCount(List l, int value)
{
	int count = 0;

	while (l)
	{
		if (l->value == value)
			count++;
		l = l->next;
	}
	return count;
}
int List_Length(List l)
{
	int i = 0;
	while (l)
	{
		i++;
		l = l->next;
	}
	return i;
}
float List_Average(List l)
{
	float sum = 0;
	int count = 0;

	while (l)
	{
		sum += l->value;
		count++;
		l = l->next;
	}
	return sum / count;
}
List Lists_Subtract(List l1, List l2 )
{
	List lr = 0, lh2 = l2;
	int isUnique = 1;

	while (l1)
	{
		while (l2)
		{
			if (l1->value == l2->value)
			{isUnique = 0; break;}
			l2 = l2->next;
		}
		if (isUnique == 1)
			List_AddToEnd(&lr, l1->value);
		l1 = l1->next;
		l2 = lh2;
		isUnique = 1;
	}
	return lr;
}
void List_Scanf(List *l)
{
	int num;
	do {
		scanf(" %d", &num);
			List_Add(l, num);
	} while ( !Cmd_IsEnter() );
}
void List_Scanf_ToEnd(List *l)
{
	int num;
	do {
		scanf(" %d", &num);
			List_AddToEnd(l, num);
	} while ( !Cmd_IsEnter() );
}

/*==========================================================================*/

/* List iterator script */
ListIterator List_GetIterator(List l)
{
	ListIterator li = (ListIterator)malloc(sizeof(ListIteratorStruct));
	li->head = l;
	li->current = l;

	return li;
}
int List_GetValue(ListIterator li){
	return li->current->value;
}
int List_Next(ListIterator li){
	if (li->current->next){
		li->current = li->current->next;
		return 1;
	}
	else
		return 0;
}
void List_Reset(ListIterator li)
{
	li->current = li->head;
}
void List_ForEach(ListIterator li, void(*FuncOnElem)(int))
{
	int isNext = 1;
	while(isNext)
	{
		FuncOnElem(List_GetValue(li));
		isNext = List_Next(li);
	}
	List_Reset(li);
}

/*==========================================================================*/

/* StrList type */

//StrList StrList_New(int length, char *strTab)
//{
//	if(!length) return 0;
//}
void StrList_Add(StrList *sl, char *str)
{
	StrList first = (StrList)malloc(sizeof(StrList));
	first->next = 0;
	first->string = (char*)malloc(sizeof(char)*strlen(str)+1);
	strcpy(first->string, str);

	first->next = (*sl);
	(*sl) = first;
}
void StrList_Print(StrList sl)
{
	while (sl)
	{
		printf("%s\n", sl->string);
		sl = sl->next;
	}
}
void StrList_Free(StrList sl)
{
	StrList tmp = sl;

	while (tmp)
	{
		tmp = tmp->next;
		free(sl->string);
		//free(sl);
		sl = tmp;
	}
}

/*==============================================================CYCLIC=LIST=*/

ListC ListC_New(int length, int data[])
{
	int i = 0;
	ListC lHead = 0, l = 0;

	if(!length) return 0;

	lHead = ListElem_New(data[i++], 0); length--;
	l = lHead;

	while(length--)
	{
		l->next = ListElem_New(data[i++], 0);
		l = l->next;
	}

	l->next = lHead;
	return lHead;
}
void ListC_Free(ListC lc)
{
	if(lc == 0) return;
	ListC lhead = lc;
	do {
		lc = lc->next;
		free(lc);
	} while(lc == lhead);
}
void ListC_Add(ListC *l ,int value)
{
	ListC ls = *l, lHead = *l;
	if(!(*l)) // jeśli lista pusta
	{
		*l = ListElem_New(value, 0);
		(*l)->next = *l;
	}
	else
	{
		while( ls->next != lHead )
			ls = ls->next;
		ls->next = ListElem_New(value, *l);
		*l = ls->next;
	}
}
void ListC_Delete(ListC *l, int value)
{
	ListElem *lhead = *l, *lp = *l, *ll = *l;
	if (!lhead) return;
	if(ll->value == value)	// jeśli głowa do usunięcia
	{
		while(lp->next != lhead)
			lp = lp->next;
		lp->next = lhead->next;
		ListElem_Delete(*l);
		*l = lp == *l ? 0 : lp->next;
	}
	else
	{	// jeśli nie głowa, znajdź wartość
		while(ll->value != value)
		{
			lp = ll;
			ll = ll->next;
			if (ll == lhead) return;
		}
		lp->next = ll->next;
		ListElem_Delete(ll);
		if(lp == lp->next) // jeśli pozostał jeden element
			*l = lp;	//  jest nową głową
	}
}
ListElem* ListC_Search(ListC l, int value)
{
	ListElem* lhead = l;
	while(l)
	{
		if(l->value == value) return l;
		l = l->next;
		if( lhead == l) return 0;
	}
	return l;
}
void ListC_Print(ListC l)
{
	ListElem* lhead = l;
	while(l)
	{
		printf(" %d", l->value);
		l = l->next;
		if(l == lhead)
			return;
	}
}

void ListC_Scanf(ListC *lc)
{
	int num;
	do {
		scanf(" %d", &num);
			ListC_Add(lc, num);
	} while ( !Cmd_IsEnter() );
}

int ListC_Length(ListC lc)
{
	if (lc == 0) return 0;
	ListC lh = lc;
	int length = 0;
	lc = lc->next; length++;
	while (lc != lh)
	{
		lc = lc->next;
		length++;
	}
	return length;
}

int ListsC_areSeriesEqual(ListC lc1,ListC lc2)
{
	ListC lh1 = lc1, lh2 = lc2; // zapisanie głów list
	if( lc1 == 0 || lc2 == 0) return 0;
	if (ListC_Length(lc1) != ListC_Length(lc2)) return 0;

	do
	{	// szukamy elementu lc2 równego wartośći lc1
		while (lc1->value != lc2->value)
		{
			lc2 = lc2->next;
			if (lc2 == lh2)
				return 0;
		}
		// przesuwamy się co element
		// jesli każdy był równy i osiągnięto początek
		// to znaczy że oba składają sie z takich samych ciągów
		while(lc1->value == lc2->value )
		{
			lc1 = lc1->next;
			lc2 = lc2->next;
			if(lc1 == lh1)
				return 1;
		}
	} while (lc1 == lh1);
	return 0;
}

/*==========================================================================*/

/* Bi-directional list type */

ListBiElem* ListBiElem_New(int value, ListBiElem *prev, ListBiElem *next)
{
	ListBiElem *biLe = (ListBiElem*)malloc(sizeof(ListBiElem));
	biLe->value = value;
	biLe->prev = prev;
	biLe->next = next;
	return biLe;
}

void ListBiElem_Delete(ListBiElem *bLe)
{
	//if (bLe->prev && bLe->next)
	//{
	//	bLe->prev->next = bLe->next->prev;
	//}
	free(bLe);
}

/*======================================================BI=DIRECTIONAL=LIST=*/

/* Bi-list type */
typedef ListBiElem* ListBi;

ListBi ListBi_New(int length, int data[])
{
	int i = 0;
	ListBi lHead = 0, l = 0;

	if(!length) return 0;

	lHead = ListBiElem_New(data[i++], 0, 0);length--;
	l = lHead;

	while(length--)
	{
		l->next = (ListBiElem*)ListElem_New(data[i++], 0);
		l = l->next;
	}

	l->next = lHead;
	return lHead;

}
void ListBi_Free(ListBi bl)
{
	ListBiElem* l = bl;
	while(l)
	{
		bl = l->next;
		free(l);
		l = bl;
	}
}
void ListBi_Add(ListBi *lb, int value)
{
	if(!(*lb))	// jeśli lista pusta
		*lb = ListBiElem_New(value, 0, 0);
	else
	{
		ListBiElem* lbe = ListBiElem_New(value, 0, (*lb));
		(*lb)->prev = lbe;
		(*lb) = lbe;
	}
}
ListBiElem* ListBi_Search(ListBi lb, int value)
{
	while(lb)
	{
		if(lb->value == value)
			return lb;
		lb = lb->next;
	}
	return lb;
}
void ListBi_Delete(ListBi *lb,  int value)
{
	ListBiElem* lr = ListBi_Search(*lb, value);
	if(lr)
	{
		if(lr->prev)
			lr->prev->next = lr->next;
		if(lr->next)
			lr->next->prev = lr->prev;
		if (lr == *lb)
			*lb = lr->next;
		free(lr);
	}
}
void ListBi_Print(ListBi lb)
{
	while(lb)
	{
		printf(" %d", lb->value);
		lb = lb->next;
	}
}

/*===============================================BI=DIRECTIONAL=CYCLIC=LIST=*/

/* Bi-list cyclic type */
typedef ListBiElem* ListBiC;

ListBiC ListBiC_New(int length, int data[])
{
	return 0;
}
void ListBiC_Free(ListBiC lbc)
{
	ListBiC lhead = lbc;
	if(lbc == 0) return;
	do {
		lbc = lbc->next;
		free(lbc);
	} while(lbc == lhead);
}
void ListBiC_Add(ListBiC *lbc, int value)
{
	ListBiElem* lbe = ListBiElem_New(value, 0, 0);
	if(!(*lbc))	// jeśli lista pusta
	{
		lbe->prev = lbe;
		lbe->next = lbe;
		*lbc = lbe;
	}
	else
	{
		lbe->next = (*lbc);
		lbe->prev = (*lbc)->prev;
		(*lbc)->prev = lbe;
		if(lbe->prev)
				lbe->prev->next = lbe;
		*lbc = lbe;
	}
}
ListBiElem* ListBiC_Search(ListBiC lbc, int value)
{
	ListBiElem *lhead = lbc;
	while(lbc)
	{
		if(lbc->value == value) return lbc;
		lbc = lbc->next;
		if(lbc == lhead) return 0;
	}
	return 0;
}
void ListBiC_Delete(ListBi *lb, int value)
{
	ListBiElem *lhead = *lb, *l = *lb, *ld = 0;
	if (!lhead) return;
	if (lhead->value == value)
	{
		if (lhead->next != lhead)	// jeśli usuwamy głowę i mamy elementy
		{
			lhead->prev->next = lhead->next;
			lhead->next->prev = lhead->prev;
			(*lb) = lhead->next;
		}
		else	// jeśli usuwamy głowę i nie mamy nic poza tym
			(*lb) = 0;
		ListBiElem_Delete(lhead);
	}
	else
	{
		while (l)
		{
			if (l->value == value)
			{
				ld = l->next;
				l->prev->next = l->next;
				l->next->prev = l->prev;
				ListBiElem_Delete(l);
				l = ld;
			}
			else
				l = l->next;
			if (lhead == l) break;
		}
	}
}
void ListBiC_Print(ListBiC lbc)
{
	ListBiElem *lhead = lbc;
	while(lbc)
	{
		printf(" %d", lbc->value);
		lbc = lbc->next;
		if(lbc == lhead) return;
	}
}

/*===============================================================QUEUE=LIST=*/
void ListQ_Add(List *lh, int value)
{
	ListElem *l = *lh, *lprev = 0;

	if( !l){
		*lh = ListElem_New(value, 0); return;
	}
	while( l )
	{
		if( l->value == value)
			return;
		else if( l->value > value )
		{
			lprev = l;
			l = l->next;
		}
		else
			break;
	}
	if( lprev)
		lprev->next = ListElem_New(value, l);
	else
		*lh = ListElem_New(value, *lh);
}
