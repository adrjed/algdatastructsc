#include "TreeFS.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

FSFile FSFile_New(char* name, void *handle, FSFile next)
{
	FSFile fn = malloc(sizeof(FileNode));
	fn->type = FS_FILE;
	fn->next = (FSNode*)next;
	fn->name = calloc(strlen(name), sizeof(char));
	strcpy(fn->name, name);
	fn->handle = handle;
	return fn;
}

void FSFile_Free(FSFile fn)
{
	if(!fn)return;
	//free(fn->name);
	free(fn);
}

FSDir FSDir_New(char *name, FSDir parent, FSNode *folders, FSNode *files, FSDir next)
{
	FSDir dn = malloc(sizeof(DirNode));
	dn->type = FS_DIR;
	dn->next = (FSNode*)next;
	dn->name = calloc(strlen(name), sizeof(char));
	strcpy(dn->name, name);
	dn->parent = parent;

	dn->folders.first = folders;
	if(folders){
	while(folders->next){
		folders=folders->next;}}
	dn->folders.last =  folders;

	dn->files.first = files;
	if(files){
		while(files->next){
			files=files->next;}}
	dn->files.last = files;
	return dn;
}

void FSDir_Free(FSDir dn)
{
	if(!dn)return;
	free(dn->name);
	free(dn);
}

//rekurencyjnie
void FSDir_RemoveForce(FSDir fd)
{
	if(fd == 0) return;

	FSFile fn = (FSFile)fd->files.first, fc;
	while(fn)
	{
		fc = (FSFile)fn->next;
		FSFile_Free(fn);
		fn = fc;
	}
	FSDir_RemoveForce((FSDir)fd->folders.first);
	FSDir_Free(fd);
}


void FSList_AddNode(FSList *fl, FSNode *n)
{
	if( fl->first == 0)
	{
		fl->first = n;
		fl->last = n;
	}
	else
	{
		fl->last->next = n;
		fl->last = n;
	}
}

int FSList_RemoveNode(FSDir fd, char* name, int force)
{
	FSNode *n;
	if( (n=FSList_SearchNode(&(fd->files), name)) != 0 )
		printf("plik %s ", n->name );
	else if( (n=FSList_SearchNode(&(fd->folders), name)) != 0)
		printf("folder %s", n->name);
	else
	{	printf("nie znaleziono"); return 1;}

	if(n)
	{
		if( n->type == FS_FILE )
		{
			FSFile fn = (FSFile)n;
			FSDir_DeleteFile(&(fd->files), fn); printf("usuniety\n");
			return 0;
		}
		else if( n->type == FS_DIR )
		{
			FSDir fdir = (FSDir)n;
			if( fdir->folders.first != 0 || fdir->files.first != 0)
			{
				if( force == 1 )
				{
					FSDir_RemoveForce(fd); printf("usuniety\n");
					return 0;
				}
				else if(force == 0)
				{
					printf("Nie mozna usunac niepustego wezla\n");
					return 1;
				}
			}
			else
			{
				FSDir_DeleteFolder(&(fd->folders), fdir );
				printf(" usuniety\n"); return 0;
			}
		}
	}
	return 1;
}
FSNode* FSList_SearchNode(FSList *fl, char* name)
{
	FSNode *n = fl->first;
	while(n){
		if( strcmp(n->name, name) == 0)
			return n;
		n = n->next;
	}
	return 0;
}
void FSList_Print(FSList fl, char type)
{
	FSNode *n = fl.first;
	while(n){
		printf("(%c)%s ",type, n->name);
		n = n->next;
	}
}

FSNode* FS_Search(FSDir startDir, char* name)
{
	return 0;
}



void FSDir_AddFile(FSDir d, FSFile f)
{
	FSList_AddNode(&(d->files), (FSNode*)f);
}

void FSDir_EmptyFile(FSDir d, char *name)
{
	FSFile nf = FSFile_New(name, 0, 0);
	FSList_AddNode(&(d->files), (FSNode*)nf);
}

void FSDir_DeleteFile(FSList *fl, FSFile f)
{
	FSFile curr = (FSFile)fl->first, tmp;
	if( curr == f )
	{
		if( curr == (FSFile)fl->last) fl->last = 0;
		fl->first = fl->first->next;
		FSFile_Free(curr);
		return;
	}

	while(curr->next)
	{
		if( (FSFile)curr->next == f )
		{
			if(curr->next == fl->last) fl->last = (FSNode*)curr;
			tmp = (FSFile)curr->next;
			curr->next = curr->next->next;
			FSFile_Free(tmp);
			return;
		}
		curr = (FSFile)curr->next;
	}

}
void FSDir_AddFolder(FSDir d, char* name)
{
	FSDir nd = FSDir_New(name, d, 0, 0, 0);
	FSList_AddNode(&(d->folders), (FSNode*)nd);
}
void FSDir_DeleteFolder(FSList *fl, FSDir dd)
{
	FSFile curr = (FSDir)fl->first, tmp;
	if( curr == dd )
	{
		if( curr == (FSDir)fl->last) fl->last = 0;
		fl->first = fl->first->next;
		FSDir_Free(curr);
		return;
	}

	while(curr->next)
	{
		if( (FSDir)curr->next == dd )
		{
			if(curr->next == fl->last) fl->last = (FSNode*)curr;
			tmp = (FSFile)curr->next;
			curr->next = curr->next->next;
			FSDir_Free(tmp);
			return;
		}
		curr = (FSDir)curr->next;
	}
}

void FSDir_PrintPath(FSDir fd)
{
	if(fd->parent != 0)
		FSDir_PrintPath(fd->parent);
	printf("%s/", fd->name);
}
void FSDir_List(FSDir fd, FSType t, int r)
{
	if( fd == 0 ) return;

	printf("\nFolder %s :\n", fd->name);
	if( t == FS_FILE || t == FS_ALL)
	{
		//printf("\tPliki:\n\t");
		FSList_Print(fd->files, 'f');
	}
	if( t == FS_DIR || t == FS_ALL )
	{
		//printf("\tFoldery:\n\t");
		FSList_Print(fd->folders, 'd');
	}
	if( r == 1 )
	{
		FSDir d = (FSDir)fd->folders.first;
		while(d)
		{
			FSDir_List(d, t, r);
			d = (FSDir)d->next;
		}
	}
	puts("");
}

void FS_GoToDir(FSDir *d, char* dirname)
{
	if( strcmp(dirname, "..") == 0)
	{
		FS_GoUp(d); return;
	}
	FSNode *nd = FSList_SearchNode(&((*d)->folders), dirname);
	if( nd )
		*d = (FSDir)nd;
	else
		puts("Nie ma takiego folderu");
}
void FS_GoTo(FSDir d, char* path);
void FS_GoUp(FSDir *d)
{
	if((*d)->parent == 0)
		puts("Jestes w korzeniu /");
	else
		*d = (*d)->parent;
}
