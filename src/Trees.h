/**
 * @file ajextra.h
 * @author Adrian Jędrzejak
 * @brief Funkcje i makra wspomagające debugowanie i poprawiające czytelność kodu
 *	oraz pewne funkcje rozszerzające
 */

#ifndef AJ_TREES_H
#define AJ_TREES_H

/*=============================================================TREE=ELEMENT=*/

/* Struktura elementu drzewa binarnego */
typedef struct _te {
	struct _te* left;
	struct _te* right;
	int value;
} TreeNode;

/* Struktura elementu drzewa binarnego z rodzicem */
typedef struct _tep {
	struct _te* left;
	struct _te* right;
	int value;
	struct _te* parent;
} TreePNode;

TreeNode* TreeNode_New(int value, TreeNode *left, TreeNode *right);
void TreeNode_Delete(TreeNode *tn);

typedef struct _tme {
	int none;
} TreeMLeaf;

/*=====================================================================TREE=*/
/* Rozmieszczenie elmentów - mniejsze na prawo, większe na lewo */

/* Typ drzewa binarnego - drzewo z lewą i prawą gałęzią */
typedef TreeNode* Tree;
/* Tworzy strukturę drzewa z wartościami z podanej tablicy */
Tree Tree_New(int elemCount, int elemData[]);
/* Czyści wszystkie elementy drzewa, od liści w górę */
void Tree_Free(Tree t);
/* Usuwa pierwszy element o podanej wartości i porządkuje
 * drzewo po usunięciu elementu */
void Tree_Delete(Tree *t,int value);
/* Dodaje liść o podanej wartości do drzewa */
void Tree_Add(Tree *t, int value);
/* Drukuje zawartość drzewa od lewej do prawej */
void Tree_Print(Tree t);
/* Drukuje zawartość drzewa zgodnie z jego strukturą */
void Tree_PrintStruct(Tree t);
/* Znajduje w drzewie element z podaną wartością */
Tree Tree_Search(Tree t,int value);
/* Zwraca następnik elementu tElem w kolejności rosnącej */
Tree Tree_Successor(Tree t, TreeNode *tElem);
/* Zwraca poprzednika elementu tElem malejąco */
Tree Tree_Predecessor(Tree t, TreeNode *tElem);
/* Najmniejszy element z posortowanego drzewa binarnego */
int Tree_Min(Tree t);
Tree Tree_MinNode(Tree t);
/* Największy element z posortowanego drzewa binarnego */
int Tree_Max(Tree t);
Tree Tree_MaxNode(Tree t);
/* Zwraca sumę elementów w drzewie */
int Tree_Sum(Tree t);
/* Wczytuje skrukturę drzewa z pliku .tree */
Tree Tree_Load(char* filename);
/* Zapisuje drzewo do pliku .tree */
void Tree_Save(Tree t, char* filename);
/* Sprawdza czy ciągi posiadają te same wartości,
 * niekoniecznie tak samo rozmieszczone w drzewie */
int Tree_AreEqualValues(Tree lt, Tree rt);

void Tree_Scanf(Tree *t);

/*============================================================TREE=ITERATOR=*/

typedef struct _tIter
{
	Tree head;
	TreeNode *currParent;
	TreeNode *current;
} TreeIteratorStruct;

typedef TreeIteratorStruct* TreeIterator;

TreeIterator Tree_GetIterator(Tree t);
int 	Tree_GetValue(TreeIterator ti);
int 	Tree_Next(TreeIterator ti);
void 	Tree_Reset(TreeIterator ti);
void 	Tree_ForEach(TreeIterator ti, void (*FuncOnElem)(int));

/*===================================================================TREE=3=*/

typedef struct tree3_s
{
	float value;
	struct tree3_s*	left;
	struct tree3_s*	mid;
	struct tree3_s*	rigth;
} Tree3Node;

typedef Tree3Node* Tree3;

/* Tworzy strukturę drzewa z wartościami z podanej tablicy */
Tree3 Tree3_New(int elemCount, int elemData[]);
/* Czyści wszystkie elementy drzewa, od liści w górę */
void Tree3_Free(Tree3 t);
/* Usuwa pierwszy element o podanej wartości i porządkuje
 * drzewo po usunięciu elementu */
void Tree3_Delete(Tree3 *t,int value);
/* Dodaje liść o podanej wartości do drzewa */
void Tree3_Add(Tree3 *t, int value);
/* Drukuje zawartość drzewa od lewej do prawej */
void Tree3_Print(Tree3 t);
/* Znajduje w drzewie element z podaną wartością */
Tree3Node* Tree3_Search(Tree3 t,int value);

#endif//AJ_TREES_H

