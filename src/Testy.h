/**
 * @file ajextra.h
 * @author Adrian Jędrzejak
 * @brief Nagłówek z kilkoma funkcjami testowymi dla wcześniej stworzynych
 *  struktur
 */

#ifndef AJ_TESTY_H
#define AJ_TESTY_H

/* ListsTests.h */
void Test_List_ForEach();

/* OnpTests.h */
int Test_Onp(int argc, char** argv);
int Test_ConvertToOnp(int argc, char** argv);


#endif//AJ_TESTY_H
