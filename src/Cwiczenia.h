/**
 * @file command.h
 * @author Adrian Jędrzejak
 * @brief Nagłówki z rozwiazaniami pierwszych zadan z cwiczen ASD.
 */

#ifndef AJ_CWICZENIA_H
#define AJ_CWICZENIA_H

int Cw1Main(int argc, char** argv);

int Cw2Main(int argc, char **argv);

int Cw3Main(int argc, char **argv);

//int Cw4Main(int argc, char **argv);

#endif//AJ_CWICZENIA_H
