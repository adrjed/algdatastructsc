/* Ćwiczenia 1 - 3.10.2014 */
#include <stdio.h>
#include <stdlib.h>

#include "ajextra.h"
#include "Lists.h"
#include "Tables.h"

#define TAB_LEN 100

/*
 * Ćw 1.1. (1) Napisać funkcję, która oblicza średnią z elementów listy niepustej
 */
void Cw1Zad1()
{
	int *it = IntTab_New_Rand(TAB_LEN, 0, 100);
	List l = List_New(TAB_LEN, it);

	List_Print(l);
	printf("\nSrednia: %.4f\n", List_Average(l));

	free(it);
	List_Free(l);
}

/*
 * Ćw 1.2. (2) Napisać funkcję dodawania do listy L1 listy L2
 */
void Cw1Zad2()
{
	int *it1 = IntTab_New_Rand(10, 0, 10);
	int *it2 = IntTab_New_Rand(10, 50, 60);
	List l1 = List_New(10, it1);
	List l2 = List_New(10, it2);

	printf("Lista 1:\n");
	List_Print(l1);
	printf("Lista 2:\n");
	List_Print(l2);
	printf("Lista po polaczeniu l1 i l2:\n");
	List_MergeToEnd(&l1, &l2);
	List_Print(l2);

	free(it1);free(it2);
	List_Free(l1);
	List_Free(l2);
}

/*
 * Ćw 1.3. (3) Napisać funkcję, która tworzy i kopiuje listę L2 do odpowiedniego
 *	indeksu pod listę L1.
 */
void Cw1Zad3()
{
	int *it1 = IntTab_New_Rand(10, 0, 10);
	int *it2 = IntTab_New_Rand(10, 50, 60);
	List l1 = List_New(10, it1);
	List l2 = List_New(10, it2);

	printf("Lista 1:\n");
	List_Print(l1);
	printf("\nLista 2:\n");
	List_Print(l2);
	printf("\nLista 1 po dolaczeniu l2:\n");
	List_MergeStartAt(&l1, l2, 4);
	List_Print(l1);

	free(it1);free(it2);
	List_Free(l1);
	List_Free(l2);
}

/*
 * Ćw 1.4. (4) Napisz funkcję, która zwraca ilość występowania wartości na
 *	liście
 */
void Cw1Zad4()
{
	int len = 10;
	int search = int_RandMM(0,10);
	int *it = IntTab_New_Rand(len, 0, 10);
	List l = List_New(len, it);

	printf("Lista:\n");
	List_Print(l);
	printf("\nSzukana wartosc: %d\n", search);
	printf("Ilosc znalezionych: %d\n", List_ValueCount(l, search));

	free(it);
	List_Free(l);
}

/*
 * Ćw 1.5. (5) Funkcja usuwająca wszystkie elementy o danej wartości
 */
void Cw1Zad5()
{
	int len = 10;
	int numdel = int_RandMM(0,10);
	int *it = IntTab_New_Rand(len, 0, 10);
	List l = List_New(len, it);

	printf("Lista:\n");
	List_Print(l);
	printf("\nWartosc do usuniecia: %d", numdel);
	printf("Lista po usunieciu wartosci:\n");
	List_RemoveValuesOf(&l, numdel);
	List_Print(l);

	free(it);
	List_Free(l);
}

/*
 * Ćw 1.6. (6) Funkcja sortująca elementy na liście rosnąco
 */
void Cw1Zad6()
{
	int len = 10;
	int *it = IntTab_New_Rand(len, 0,10);
	List l = List_New(len, it);

	printf("Lista:\n");
	List_Print(l);
	printf("\nLista po sortowaniu rosnacym:\n");
	List_Sort(&l, 1);
	List_Print(l);
	printf("\nLista po sortowaniu malejacym:\n");
	List_Sort(&l, 0);
	List_Print(l);

	free(it);
	List_Free(l);
}

int Cw1Main(int argc, char** argv)
{
	//Cw1Zad1();
	//Cw1Zad2();
	//Cw1Zad3();
	//Cw1Zad4();
	//Cw1Zad5();
	Cw1Zad6();
	return 0;
}
