/**
 * @file TreeFS.h
 * @author Adrian Jędrzejak
 * @brief Struktury i funkcje drzew folderów i plików
 */

#ifndef AJ_TREE_FS_H
#define AJ_TREE_FS_H

/**=============================================================FOLDER=TREE=*/

enum fstype_e
{
	FS_FILE = 1 << 0,        //!< FS_FILE
	FS_DIR = 1 << 1,         //!< FS_DIR
	FS_ALL = FS_FILE | FS_DIR//!< FS_ALL
};
typedef enum fstype_e FSType;

typedef struct fsnode_s FSNode;
/** FSNode
 * Struktura bazowa dla DirNode i FileNode
 * służąca do wyciagania informacji o typie i nazwie
 * Coś na wzór klasy bazowej
 */
typedef struct fsnode_s
{
	FSType	type;	// typ węzła
	FSNode	*next;	// kolejny węzeł tego samego typu
	char	*name;	// nazwa węzła
} FSNode;

/**
 * Lista połączona kolejnych węzłów tego samego typu
 */
typedef struct fslist_s
{
	FSNode *first;
	FSNode *last;
} FSList;

/** FileNode : FSNode
 * Reprezentacja węzła pliku systemu plików
 */
typedef struct filenode_s
{
	FSType	type;	//>
	FSNode	*next;	//> część wspólna z FSNode
	char	*name;	//>

	void 	*handle;// uchwyt do pliku
} FileNode;

typedef FileNode* FSFile;

/**
 * Tworzy nowy węzeł pliku w systemie
 * @param name - nazwa pliku
 * @param handle - uchwyt do danych
 * @param next - następny plik w liście
 * @return
 */
FSFile FSFile_New(char* name, void* handle, FSFile next);

typedef struct dirnode_s DirNode;
typedef DirNode* FSDir;
/** DirNode : FSNode
 * Reprezentacja folderu systemu plików
 */
typedef struct dirnode_s
{
	FSType	type;	//>
	FSNode	*next;	//> część wspólna z FSNode
	char	*name;	//>

	FSDir	parent; // folder zawierający
	FSList	folders;// lista podfolderów
	FSList 	files;  // lista plików
} DirNode;

/**
 * Tworzy nowy węzeł folderu w systemie
 * @param name - nazwa węzła
 * @param parent - folder do którego należy węzeł
 * @param folders - foldery należące do węzła
 * @param files - pliki należące do węzła
 * @param next - następny folder w liście
 * @return - wskaźnik na nowo utworzony węzeł
 */
FSDir FSDir_New(char *name, FSDir parent, FSNode *folders, FSNode *files, FSDir next);

FSNode* FS_SearchNode(FSNode sNode, char* name);
FSNode* FSList_SearchNode(FSList *fl, char* name);
int FSList_RemoveNode(FSDir fd, char* name, int force);
void FSList_Print(FSList fl, char type);

/**
 * Zwraca pierwszy znaleziony węzeł o danej nazwie. Na podstawie FSNode->type
 * sprawdzamy czy jest to plik, czy folder
 * @param startDir - folder, z którego zaczynamy szukać
 * @param name - szukana nazwa węzła
 * @return - typ FSNode znalezionego węzła
 */
FSNode* FS_Search(FSDir startDir, char* name);

void FSDir_PrintPath(FSDir fd);
void FSDir_AddFile(FSDir d, FSFile f);
void FSDir_EmptyFile(FSDir d, char *name);

void FSDir_DeleteFile(FSList *fl, FSFile f);
void FSDir_AddFolder(FSDir d, char* name);
void FSDir_DeleteFolder(FSList *fl, FSDir dd);

/**
 * Wyświetla zawartość danego węzła, jeśli jest katalogiem
 * @param fd - folder, którego zawartość wyświetlamy
 * @param t - [FS_DIR|FS_FILE|FS_ALL] typ wyświetlanych węzłów
 * @param r - [0|1] wyświetlanie rekursywne
 */
void FSDir_List(FSDir fd, FSType t, int r);

void FS_GoToDir(FSDir *d, char* dirname);
void FS_GoTo(FSDir d, char* path);
void FS_GoUp(FSDir *d);

#endif//AJ_TREE_FS_H
