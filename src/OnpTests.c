/* Funkcje testujące parsowanie wyrażeń w odwrotnej notacji polskiej ONP */

#ifndef ONP_TESTS_C
#define ONP_TESTS_C

#include "stdio.h"
#include "Stack.h"
#include "ONP.h"

int Test_Onp(int argc, char** argv)
{
	char input[256] = {'\0'};
	int out = 0;
	Stack s = 0;

	puts("Wprowadz wyrazenie ONP do przetworzenia");

	do
	{
		if(!input[0])
			puts("Podaj wyrazenie:");
		scanf(" %s", input);
		out = ONP_ParseString(&s, input);
		printf("| "); Stack_PrintUp(s); printf("< [%d]\n", out);
	}while(input[0] != '=');

	return 0;
}

int Test_ConvertToOnp(int argc, char** argv)
{
	char input[256] = {0}, output[256] = {0};
	int i, o, out, isExit;
	Stack s = 0;

	printf("Wprowadz wyrazenie arytmetyczne do konwersji na ONP");
	do
	{
		i = 0; o = 0; out = 0; isExit = 0;
		do
		{
			if(!input[0])
				printf("\nPodaj wyrazenie arytmetyczne: ");
			scanf(" %s", input);
			if(input[0] == 'Q')
			{
				isExit = 1;
				break;
			}
			ONP_ConvertToOnp(&s, input, output);
		}while(input[0] != '=');

		//puts("Zwykle wyrazenie:");
		//printf(" %s", input);
		puts("Wyrazenie ONP:");
		printf(" %s", output);

		Stack_Free(&s);
		while(output[o])output[o++]=0;
		while(input[i]) input[i++]=0;
	}while(!isExit);
	/*puts("Przetwarzanie wyrazenia ONP:");
	while(output[o])
	{
		out = ONP_ParseString(&s, output+o);
		while(output[o] != ' ')
			o++;
		printf("| "); Stack_PrintRev(s); printf("< [%d]\n", out);
	}*/

	return 0;
}

#endif//ONP_TESTS_C
