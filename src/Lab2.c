/* Laboratorium 2 - 13.10.2014 */
#include <stdio.h>
#include <malloc.h>

#include "Lists.h"
#include "Tables.h"

#include "diag.h"

#undef DIAG

/* Lab 2.1. (4) Odwracanie elementow listy. W pierwszym wariancie odwróć elementy
 w istniejącej liście, w drugim stwórz odwróconą kopię listy
 *
 * Funkcje stworzone w tym zadaniu:
 * 	List_Reverse_Old
 * 	List_ReverseRec
 * 	List_ReversedCopy
 */
void List_Reverse_Old(List *l)
{
	List lprev = 0, lnext = 0;

	if (!(*l)) return;

	lprev = *l;
	(*l) = (*l)->next;
	lprev->next = 0;

	if (!(*l)){(*l) = lprev;return;}

	while ((*l))
	{
		lnext = (*l);
		(*l) = (*l)->next;

		lnext->next = lprev;
		lprev = lnext;
	}

	(*l) = lprev;
}

List List_ReversedCopy(List l)
{
	List llast = 0, lnext = 0;

	if (!l) return 0;
	llast = (List)malloc(sizeof(ListElem));
	llast->value = l->value;
	llast->next = 0;
	l = l->next;

	if (!l) return llast;
	while (l)
	{
		lnext = (List)malloc(sizeof(ListElem));
		lnext->value = l->value;

		lnext->next = llast;
		llast = lnext;

		l = l->next;
	}

	return lnext;
}

#ifdef DIAG
void Lab2Zad1(int len)
{
	int *it = IntTab_New_Rand(len, 0, 5);
	List l = List_New(len, it), lr = 0;

	DIAG_CLOCK_START
	lr = List_ReversedCopy(l);
	List_Reverse_Old(&l);
	DIAG_CLOCK_STOP

	free(it);List_Free(l);List_Free(lr);
}
#else
void Lab2Zad1(int len)
{
	int *it = IntTab_New_Rand(len, 0, 5);
	List l = List_New(len, it), lr = 0;

	printf("\n2.1. Wyniki:\n");
	printf("Losowa lista:\n");
	List_Print(l);
	printf("\nOdwrocona kopia listy:\n");
	lr = List_ReversedCopy(l);
	List_Print(lr);
	printf("\nOdwrocenie bez kopiowania\n");
	List_Reverse_Old(&l);
	List_Print(l);

	free(it);List_Free(l);List_Free(lr);
}
#endif

/* Lab 2.2. (5) Mamy dwie listy posortowane malejąco. Stwórz nową posortowaną
 *	listę na podstawie dwóch, a w drugim wariancie połącz dwie listy w jedną
 *
 * Funkcje stworzone w tym zadaniu:
 * 	List_GetMergedAndSorted
 * 	List_MergeWithSort
 */
// gdy obie listy posortowane malejąco
List List_GetMergedAndSorted(List l1, List l2)
{
	List lHead = 0, l = 0;
	int value = 0, first = 0;

	while (l1 || l2)
	{
		if (l1 && l2)
		{
			if (l1->value > l2->value){
				value = l1->value;
				l1 = l1->next;
			}
			else{
				value = l2->value;
				l2 = l2->next;
			}
		}
		else if (l1){
			value = l1->value;
			l1 = l1->next;
		}
		else if (l2){
			value = l2->value;
			l2 = l2->next;
		}

		if (first){
			l->next = ListElem_New(value, 0);
			l = l->next;
		}
		else{
			lHead = ListElem_New(value, 0);
			l = lHead;
			first = 1;
		}
	}

	return lHead;
}

// gdy obie listy są posortowane malejąco
void List_MergeWithSort(List *l1, List *l2)
{
	List lTmp = 0, lHead = 0;

	if (!(*l1) || (!*l2))
	{
		lHead = (*l1) > (*l2) ? (*l1) : (*l2);
		(*l1) = lHead;
		(*l2) = lHead;
		return;
	}

	// sprawdzenie, która wartość początkowa jest większa
	if ((*l1)->value < (*l2)->value)
	{
		lHead = (*l2);	//lHead robi za pomocniczą
		(*l2) = (*l1);
		(*l1) = lHead;	//mamy glowe listy z najwieksza wartoscia
	}
	else
		lHead = (*l1);  //jeśli nie potrzeba zamiany, tylko lHead

	while ( (*l2) )
	{
		if (!(*l1)->next)
		{
			(*l1)->next = (*l2);
			break;
		}
		else if ((*l1)->next->value < (*l2)->value)
		{
			lTmp = (*l2)->next;
			(*l2)->next = (*l1)->next;
			(*l1)->next = (*l2);
			(*l2) = lTmp;
		}
		(*l1) = (*l1)->next;
	}

	(*l1) = lHead;
	(*l2) = lHead;
}

#ifdef DIAG
void Lab2Zad2(int l1len, int l2len)
{
	int len = 1,
		*it1 = IntTab_New_Desc(l1len, 1),
		*it2 = IntTab_New_Desc(l2len, 2);
	List l1 = List_New(l1len, it1);
	List l2 = List_New(l2len, it2);
	List lw = 0;

	DIAG_CLOCK_START
	lw = List_GetMergedAndSorted(l1, l2);
	List_MergeWithSort(&l1, &l2);
	DIAG_CLOCK_STOP

	free(it1); free(it2);
	List_Free(l1);List_Free(lw);
}
#else
void Lab2Zad2(int l1len, int l2len)
{
	int len = 1,
		*it1 = IntTab_New_Desc(l1len, l1len),
		*it2 = IntTab_New_Desc(l2len, l2len);
	List l1 = List_New(l1len, it1);
	List l2 = List_New(l2len, it2);
	List lw = 0;

	printf("Lista 1:\n");
	List_Print(l1);
	printf("\nLista 2:\n");
	List_Print(l2);
	printf("\nNowa lista polaczona:\n");
	lw = List_GetMergedAndSorted(l1, l2);
	List_Print(lw);
	printf("\nLista polaczona z dwoch:\n");
	List_MergeWithSort(&l1, &l2);
	List_Print(l2);

	free(it1); free(it2);
	List_Free(l1);List_Free(lw);
}
#endif

#ifdef DIAG
int Lab2Main(int argc, char **argv)
{
	int STEPS = 40,
		START = 4,
		REPEATS = 10;
	float MULTIPLER = 1.25;
	FILE *f = 0;

	printf("/* Laboratorium 1 - 6.10.2014 - Adrian Jedrzejak (267531) */\n");
	printf("/* Pomiar zlozonosci czasowej dla ustalonej serii danych /*\n\n");

	printf("\n== POMIAR DLA ZADANIA 2.1. ==\n");
	Diag_ConfigTest(&START, &MULTIPLER, &STEPS, &REPEATS);
	f = Diag_OpenFile("Lab2Zad1Diag.csv");
	DIAG_DATA_LOOP_START(START, MULTIPLER, STEPS, REPEATS)

		Lab2Zad1(DiagDataLength); // ZADANIE 2.1.
		DIAG_CHECK_CLOCK

		DIAG_DATA_SERIES_END
		printf("2.1. Step: %2d Length: %-10d Time: %-7.2f ms\n",DiagStep, DiagDataLength, DIAG_CHECK_AVERAGE);
		Diag_AddResult(f, DiagInfo);
	DIAG_DATA_LOOP_END
	Diag_CloseFile(f);
	printf("\n== KONIEC POMIARU DLA ZADANIA 2.1. ==\n");


	printf("\n== POMIAR DLA ZADANIA 2.2. ==\n");
	Diag_ConfigTest(&START, &MULTIPLER, &STEPS, &REPEATS);
	f = Diag_OpenFile("Lab2Zad2Diag.csv");
	DIAG_DATA_LOOP_START(START, MULTIPLER, STEPS, REPEATS)

		Lab2Zad2(DiagDataLength, DiagDataLength); // ZADANIE 2.2.
		DIAG_CHECK_CLOCK

		DIAG_DATA_SERIES_END
		printf("2.2. Step: %2d Length: %-10d Time: %-7.2f ms\n",DiagStep, DiagDataLength, DIAG_CHECK_AVERAGE);
		Diag_AddResult(f, DiagInfo);
	DIAG_DATA_LOOP_END
	Diag_CloseFile(f);
	printf("\n== KONIEC POMIARU DLA ZADANIA 2.2. ==\n");

	printf("\nKoniec pomiarow  Lab 2 - nacisnij klawisz...");
	getchar();
	return 0;
}
#else
int Lab2Main(int argc, char **argv)
{
	int length = 0, l1 = 0, l2 = 0;

printf("/* Laboratorium 2 - 13.10.2014 - Adrian Jedrzejak (267531) */\n\n");

	// ZADANIE 2.1.
printf("\
/* Lab 2.1. (4) Odwracanie elementow listy. W pierwszym wariancie odwroc elementy\n\
 *  w istniejacej liscie, w drugim stworz odwrocona kopie listy\n\
 */\n");
printf("> Podaj dlugosc losowej tablicy: "); scanf(" %d", &length);
fflush(stdin);
	Lab2Zad1(length);

	// ZADANIE 2.2.
printf("\n\n\
/* Lab 2.2. (5) Mamy dwie listy posortowane malejaco. Stworz nowa posortowana\n\
 *	liste na podstawie dwoch, a w drugim wariancie polacz dwie listy w jedna\n\
 */\n");
printf("> Podaj dlugosc 1 i 2 tablicy malejacej: "); scanf(" %d %d", &l1, &l2);
fflush(stdin);
	Lab2Zad2(l1, l2);

printf("\nKoniec Lab 2 - nacisnij klawisz...");
fflush(stdin); getchar(); 
	return 0;
}
#endif
