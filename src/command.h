/**
 * @file command.h
 * @author Adrian Jędrzejak
 * @brief Możliwość używania komend wieloznakowych
 */

#ifndef AJ_COMMAND_H
#define AJ_COMMAND_H

//=================================================================SAFE=READ=/

#ifndef _MSC_VER
#define scanf_s( fmt, ... ) scanf( scanf_validate( fmt, __FILE__, __LINE__ ), __VA_ARGS__ )
#endif

//===============================================================COMMAND=TAB=/

/**
 * Wyliczenie możliwych do użycia komend.
 * Do zastosowania w instrukcjach switch case.
 * Lista potrzebnych elementów wyliczenia może być rozbudowana. Należy
 * pamiętać o uzupełnieniu inicjacji funkcji Cmd_Init()
 */
enum cmdtype_e
{
	CMD_ERROR = -1,	// brak danej komendy
	CMD_OK = 0,		// brak błędów
	CMD_EXIT,		// wyjście ze stanu
	CMD_ADD,		// dodanie
	CMD_CD,			// zmiana katalogu
	CMD_COPY,		// skopiowanie
	CMD_CREATE,		// utworzenie np. pliku
	CMD_EDIT,		// edycja
	CMD_FIND,		// szukanie
	CMD_GET,		// pobranie
	CMD_HELP,		// pomoc programu
	CMD_LIST,		// wylistowanie
	CMD_LOAD,		// wczytywanie
	CMD_MKDIR,		// utworzenie katalogu
	CMD_MOVE,		// przemieszczenie
	CMD_REMOVE,		// usunięcie
	CMD_SAVE,		// zapisywanie
	CMD_TOUCH,		// tworzenie pliku
	CMD_LENGTH		// ostatnia wartość służy jako informacja do inicjowania
					// tablicy ciągów znakowych komend. Nie używać jak pozostałe
};
typedef enum cmdtype_e CmdType;

/**
 * Globalna lista komend z przypisanymi im ciągami znaków
 */
char *CmdList[CMD_LENGTH];

/**
 * Inicjuje globalną listę komend aliasami
 */
void Cmd_Init();

/**
 * Sprawdza czy kolejny znak to wciśnięty ENTER -'\n'.
 * Jeśli nie to pobrany znak zostaje zwrócony do stdin
 * @retval 1 - wciśnięto ENTER
 * @retval 0 - nie wciśnięto ENTER
 */
int Cmd_IsEnter();

/**
 * Pobiera literę opcji w stylu '-p'
 * @return - znak opcji
 * @retval 0 - brak opcji
 */
char Cmd_GetOpt();

/**
 * Zwraca indeks z wyliczenia CmdType wpisanej komendy
 * @param cmd - ciąg znaków komendy, dla której sprawdzamy indeks
 * @return - indeks danej komendy
 * @retval CMD_ERROR (-1) - nie znaleziono komendy
 */
int Cmd_GetID(char* cmd);

/**
 * Czyta komendę od użytkownika i zwraca jej indeks. Opcjonalnie
 * można pobrać wprowadzoną komendę przez argument cmd
 * @param cmd - 0 jeśli niepotrzebne. Wskaźnik będzie pokazywać na
 *  wpisaną komendę
 * @return - indeks komendy
 */
int Cmd_Read(char* cmd);

#endif//AJ_COMMAND_H
