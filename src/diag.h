/**
 * @file diag.h
 * @author Adrian Jędrzejak
 * @brief Nagłówek diag.h służy do diagnostyki kodu programu:
 * 	- dokładny pomiar wykonania się wyznaczonej sekcji kodu
 * 	- pomiar statystyczny - uśrednianie kilku pomiarów tego samego kodu
 * 	- zależność pamięć/czas - badanie w kilku przebiegach czasu wykonania
 * 		kawałka kodu dla różnych rozmiarów danych wejściowych
 */

#ifndef AJ_DIAG_H
#define AJ_DIAG_H

/*===================================================MULTI-PLATFORM=SUPPORT=*/

#ifdef _WIN32
#	include <Windows.h>
#elif __linux
#	include <time.h>
#endif

/* Diagnostyka aktywna na Windows, Linux */
#if _WIN32 | __linux
#define DIAG
#endif

/* Zgodność sktruktur Windows.h z Linux */
#ifdef __linux
typedef unsigned long DWORD;
typedef long LONG;

#define CLOCK_REALTIME		0

//#if !defined(_M_IX86)
// typedef __int64 LONGLONG;
//#else
 typedef double LONGLONG;
//#endif

typedef union _LARGE_INTEGER {
  struct {
    DWORD LowPart;
    LONG  HighPart;
  };
  struct {
    DWORD LowPart;
    LONG  HighPart;
  } u;
  LONGLONG QuadPart;
} LARGE_INTEGER, *PLARGE_INTEGER;

typedef struct timespec diagTime_t;
typedef struct timespec diagFreq_t;
#elif _WIN32
typedef LARGE_INTEGER diagTime_t;
typedef LARGE_INTEGER diagFreq_t;
#endif//__linux

/*====================================================DIAGNOSTIC=STRUCTURES=*/

typedef struct _diagElapsed{
	double ns;
	double ms;
} diagElapsed_t;

typedef struct _diag{
	diagTime_t start;
	diagTime_t end;
	diagFreq_t frequency;
	diagElapsed_t elapsed;
} diag_t;

typedef struct _diagInfo{
	int step;
	int repeats;
	int length;
	diagElapsed_t elapsed;
} diagInfo_t;

typedef diag_t* Diag;

/*=======================================================DIAGNOSTIC=GLOBALS=*/

#ifdef DIAG

/** Globalna zmienna przechowująca wyniki działania zdefiniowanych makr */
diag_t DiagGlobal;
/** Zmienna do wykorzystania przy wyłączaniu użycia kawałka kodu podczas testów*/
int DiagMode;

diagInfo_t DiagInfo;

double DiagTimeSum;
double DiagTimeAverage;
/** Ilość danych wejściowych w serii */
int DiagDataLength;
float fDiagDataLength;
/** Mnożnik, o ile ma zwiększać się ilość danych co serię */
float DiagDataMultipler;
/** Numer serii pomiarowej */
int DiagStep;
/** Numer powtórzenia w danej serii pomiarowej */
int DiagRepeat;

/*=====================================================DIAGNOSTIC=FUNCTIONS=*/
/**
 * Funkcje diagnostyczne mogą zostać wykorzystane dla lokalnie zdefiniowanych
 * 	zmiennych Diag. Korzystają z nich makra, lecz z wstępnie zdefiniowaną
 * 	globalną strukturą DiagGlobal.
 */

/** Uzyskanie nowego wskaźnika na strukturę diag */
Diag Diag_New();
void Diag_Free();
void Diag_StartClock(Diag d);
void Diag_StopClock(Diag d);
void Diag_Reset(Diag d);

void Diag_ConfigTest(int *startLength, float *multipler, int *series, int *serieRepeats);
FILE* Diag_OpenFile(char *name);
void Diag_CloseFile(FILE* f);
void Diag_AddResult(FILE* f, diagInfo_t di);

int Diag_Random(int min, int max);

/*========================================================DIAGNOSTIC=MACROS=*/
/**
 * Makra diagnostyczne służą do szybkiego definiowania badanej sekcji.
 * 	Uzyskane wyniki należy pobrać z globalnej zmiennej DiagGlobal.
 */

/** \brief Makro do szybkiego oznaczenia miejsca rozpoczącia pomiaru czasu*/
#define DIAG_CLOCK_START				\
	Diag_StartClock(&DiagGlobal);		\

/** \brief Zakończenie pomiaru czasu */
#define DIAG_CLOCK_STOP					\
	Diag_StopClock(&DiagGlobal);		\

#define DIAG_CHECK_CLOCK DiagTimeSum += DiagGlobal.elapsed.ms;

#define DIAG_CHECK_AVERAGE DiagTimeSum/DiagRepeat

/**
 * Wyznacza początek sekcji kodu, ktora będzie testowana za pomocą zdefiniowanej
 * porcji danych wejściowych
 * @param startDataLength	- Długość początkowej porcji danych wejściowych
 * @param dataMultipler		- Mnożnik o który powiększana będzie ilość danych
 * 								co serię
 * @param stepsCount		- Ilość serii pomiarowych, główna pętla
 * @param repeatStepCount	- Ilość powtórzeń serii pomiarowej, wewnętrzna pętla
 */
#define DIAG_DATA_LOOP_START(startDataLength, dataMultipler, stepsCount, repeatStepCount)\
	DiagDataMultipler = (dataMultipler);\
	DiagDataLength = (startDataLength);\
	for(DiagStep = 1; DiagStep <= (stepsCount); DiagStep++)\
		{\
		for(DiagRepeat = 1; DiagRepeat <= (repeatStepCount); DiagRepeat++)\
			{

/**
 * Wyznacza koniec serii pomiarów dla określonej ilości danych wejściowych.
 * Ilość danych zostaje zmieniona dla kolejnej serii pomiarów
 */
#define DIAG_DATA_SERIES_END\
			}\
		DiagInfo.step = DiagStep;\
		DiagInfo.repeats = DiagRepeat;\
		DiagInfo.length = DiagDataLength;\
		DiagInfo.elapsed = DiagGlobal.elapsed;\
		DiagDataLength = DiagDataLength + (int)(DiagDataMultipler); \
		//fDiagDataLength *= DiagDataMultipler;
		//DiagDataLength = (int)fDiagDataLength;

/**
 * Wyznacza koniec testowanej sekcji kodu zaczynającej się od
 * DIAG_DATA_LOOP_START
 */
#define DIAG_DATA_LOOP_END \
	DiagTimeSum = 0;\
	}

#else
#error "diag.h - only Windows and Linux supported"
#endif//DIAG

#endif//AJ_DIAG_H
