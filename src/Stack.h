/**
 * @file Stack.h
 * @author Adrian Jędrzejak
 * @brief Struktura i funkcje stosu
 */

#ifndef AJ_STACK_H
#define AJ_STACK_H

/**
 * Struktura elementu stosu
 */
typedef struct stackelem_s StackElem;
typedef struct stackelem_s
{
	int value;			// wartość
	StackElem* prev;	// element "pod"
} StackElem;

/**
 * Typ stosu
 */
typedef StackElem* Stack;

/**
 * Tworzy nowy element stosu
 * @param value - wartość
 * @param prev - wskaźnik na element pod
 * @return - adres elementu
 */
StackElem* StackElem_New(int count, StackElem* prev);

/**
 * Zwalnia pamięć elementu stosu
 * @param se - element do zwolnienia
 */
void StackElem_Free(StackElem* se);

/**
 * Tworzy stos wypełniony podanymi argumentami
 * @param height - "wysokość", długość tablicy danych
 * @param data - tablica danych
 * @return - typ stosu
 */
Stack Stack_New(int height, int* data);

/**
 * Zwalnia pamięć stosu i wszystkich jego elementów
 * @param s - stos do zwolnienia
 */
void Stack_Free(Stack* s);

/**
 * Drukuje stos od góry do dołu
 * @param s - stos do wydrukowania
 */
void Stack_PrintDown(Stack s);

/**
 * Drukuje stos rekurencyjnie od dołu do góry
 * @param s - stos do wydrukowania
 */
void Stack_PrintUp(Stack s);

/**
 * Wkłada wartość na czubek stos
 * @param s - stos, do którego dodajemy
 * @param value - dodawana wartość
 */
void Stack_Push(Stack* s, int count);

/**
 * Zwraca wartość z czubka stosu
 * @param s - stos, z którego zdejmujemy
 * @return - zdjęta wartość
 * @retval INT_MAX - Stos był pusty
 */
int Stack_Pop(Stack* s);

/**
 * Wkłada element stosu na jego czubek
 * @param s - stos, do którego dodajemy
 * @param se - dodawany element
 */
void Stack_PushElem(Stack *s, StackElem* se);

/**
 * Zwraca element z czubka stosu
 * @param s - stos, z którego zdejmujemy
 * @return - zdjęty element
 * @retval 0 - Stos był pusty
 */
StackElem* Stack_PopElem(Stack *s);

/**
 * Rozdziela stos na dwa, zdejmując daną ilość elementów i
 * zwracając tak powstały nowy stos.
 * @param s - stos, od którego oddzielimy elementy
 * @param value - ilość elementow zdjętych
 * @return - nowy stos złożony ze zdjętych elementów
 * @retval 0 - zdejmowanie z pustego stosu
 */
Stack Stack_PopFrom(Stack *s, int value);

#endif//AJ_STACK_H
