##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=AlgDataStructsC
ConfigurationName      :=Debug
WorkspacePath          := "/mnt/windows/Users/Adrian/Documents/Visual Studio 2013/Projects/STUDIA/AlgDataStructsC/ide/codelite/AlgDataStructsC"
ProjectPath            := "/mnt/windows/Users/Adrian/Documents/Visual Studio 2013/Projects/STUDIA/AlgDataStructsC/ide/codelite/AlgDataStructsC"
IntermediateDirectory  :=Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=root
Date                   :=10/24/14
CodeLitePath           :="/root/.codelite"
LinkerName             :=/usr/bin/g++ 
SharedObjectLinkerName :=/usr/bin/g++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName).exe
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="AlgDataStructsC.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  /DEBUG
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch)../../../src/ 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/ar rcu
CXX      := /usr/bin/g++ 
CC       := /usr/bin/gcc 
CXXFLAGS :=  /Zi $(Preprocessors)
CFLAGS   :=  /Zi $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/as 


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=$(IntermediateDirectory)/src_AlgDataStructsC.c$(ObjectSuffix) $(IntermediateDirectory)/src_Cw2.c$(ObjectSuffix) $(IntermediateDirectory)/src_Cw3.c$(ObjectSuffix) $(IntermediateDirectory)/src_Lab1.c$(ObjectSuffix) $(IntermediateDirectory)/src_Lab1Zad1.c$(ObjectSuffix) $(IntermediateDirectory)/src_Lab2.c$(ObjectSuffix) $(IntermediateDirectory)/src_Lists.c$(ObjectSuffix) $(IntermediateDirectory)/src_ListsTests.c$(ObjectSuffix) $(IntermediateDirectory)/src_Tables.c$(ObjectSuffix) $(IntermediateDirectory)/src_diag.c$(ObjectSuffix) \
	



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

$(IntermediateDirectory)/.d:
	@test -d Debug || $(MakeDirCommand) Debug

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/src_AlgDataStructsC.c$(ObjectSuffix): ../../../src/AlgDataStructsC.c $(IntermediateDirectory)/src_AlgDataStructsC.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/mnt/windows/Users/Adrian/Documents/Visual Studio 2013/Projects/STUDIA/AlgDataStructsC/src/AlgDataStructsC.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_AlgDataStructsC.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_AlgDataStructsC.c$(DependSuffix): ../../../src/AlgDataStructsC.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_AlgDataStructsC.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_AlgDataStructsC.c$(DependSuffix) -MM "../../../src/AlgDataStructsC.c"

$(IntermediateDirectory)/src_AlgDataStructsC.c$(PreprocessSuffix): ../../../src/AlgDataStructsC.c
	@$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_AlgDataStructsC.c$(PreprocessSuffix) "../../../src/AlgDataStructsC.c"

$(IntermediateDirectory)/src_Cw2.c$(ObjectSuffix): ../../../src/Cw2.c $(IntermediateDirectory)/src_Cw2.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/mnt/windows/Users/Adrian/Documents/Visual Studio 2013/Projects/STUDIA/AlgDataStructsC/src/Cw2.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_Cw2.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_Cw2.c$(DependSuffix): ../../../src/Cw2.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_Cw2.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_Cw2.c$(DependSuffix) -MM "../../../src/Cw2.c"

$(IntermediateDirectory)/src_Cw2.c$(PreprocessSuffix): ../../../src/Cw2.c
	@$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_Cw2.c$(PreprocessSuffix) "../../../src/Cw2.c"

$(IntermediateDirectory)/src_Cw3.c$(ObjectSuffix): ../../../src/Cw3.c $(IntermediateDirectory)/src_Cw3.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/mnt/windows/Users/Adrian/Documents/Visual Studio 2013/Projects/STUDIA/AlgDataStructsC/src/Cw3.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_Cw3.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_Cw3.c$(DependSuffix): ../../../src/Cw3.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_Cw3.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_Cw3.c$(DependSuffix) -MM "../../../src/Cw3.c"

$(IntermediateDirectory)/src_Cw3.c$(PreprocessSuffix): ../../../src/Cw3.c
	@$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_Cw3.c$(PreprocessSuffix) "../../../src/Cw3.c"

$(IntermediateDirectory)/src_Lab1.c$(ObjectSuffix): ../../../src/Lab1.c $(IntermediateDirectory)/src_Lab1.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/mnt/windows/Users/Adrian/Documents/Visual Studio 2013/Projects/STUDIA/AlgDataStructsC/src/Lab1.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_Lab1.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_Lab1.c$(DependSuffix): ../../../src/Lab1.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_Lab1.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_Lab1.c$(DependSuffix) -MM "../../../src/Lab1.c"

$(IntermediateDirectory)/src_Lab1.c$(PreprocessSuffix): ../../../src/Lab1.c
	@$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_Lab1.c$(PreprocessSuffix) "../../../src/Lab1.c"

$(IntermediateDirectory)/src_Lab1Zad1.c$(ObjectSuffix): ../../../src/Lab1Zad1.c $(IntermediateDirectory)/src_Lab1Zad1.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/mnt/windows/Users/Adrian/Documents/Visual Studio 2013/Projects/STUDIA/AlgDataStructsC/src/Lab1Zad1.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_Lab1Zad1.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_Lab1Zad1.c$(DependSuffix): ../../../src/Lab1Zad1.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_Lab1Zad1.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_Lab1Zad1.c$(DependSuffix) -MM "../../../src/Lab1Zad1.c"

$(IntermediateDirectory)/src_Lab1Zad1.c$(PreprocessSuffix): ../../../src/Lab1Zad1.c
	@$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_Lab1Zad1.c$(PreprocessSuffix) "../../../src/Lab1Zad1.c"

$(IntermediateDirectory)/src_Lab2.c$(ObjectSuffix): ../../../src/Lab2.c $(IntermediateDirectory)/src_Lab2.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/mnt/windows/Users/Adrian/Documents/Visual Studio 2013/Projects/STUDIA/AlgDataStructsC/src/Lab2.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_Lab2.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_Lab2.c$(DependSuffix): ../../../src/Lab2.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_Lab2.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_Lab2.c$(DependSuffix) -MM "../../../src/Lab2.c"

$(IntermediateDirectory)/src_Lab2.c$(PreprocessSuffix): ../../../src/Lab2.c
	@$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_Lab2.c$(PreprocessSuffix) "../../../src/Lab2.c"

$(IntermediateDirectory)/src_Lists.c$(ObjectSuffix): ../../../src/Lists.c $(IntermediateDirectory)/src_Lists.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/mnt/windows/Users/Adrian/Documents/Visual Studio 2013/Projects/STUDIA/AlgDataStructsC/src/Lists.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_Lists.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_Lists.c$(DependSuffix): ../../../src/Lists.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_Lists.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_Lists.c$(DependSuffix) -MM "../../../src/Lists.c"

$(IntermediateDirectory)/src_Lists.c$(PreprocessSuffix): ../../../src/Lists.c
	@$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_Lists.c$(PreprocessSuffix) "../../../src/Lists.c"

$(IntermediateDirectory)/src_ListsTests.c$(ObjectSuffix): ../../../src/ListsTests.c $(IntermediateDirectory)/src_ListsTests.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/mnt/windows/Users/Adrian/Documents/Visual Studio 2013/Projects/STUDIA/AlgDataStructsC/src/ListsTests.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_ListsTests.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_ListsTests.c$(DependSuffix): ../../../src/ListsTests.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_ListsTests.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_ListsTests.c$(DependSuffix) -MM "../../../src/ListsTests.c"

$(IntermediateDirectory)/src_ListsTests.c$(PreprocessSuffix): ../../../src/ListsTests.c
	@$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_ListsTests.c$(PreprocessSuffix) "../../../src/ListsTests.c"

$(IntermediateDirectory)/src_Tables.c$(ObjectSuffix): ../../../src/Tables.c $(IntermediateDirectory)/src_Tables.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/mnt/windows/Users/Adrian/Documents/Visual Studio 2013/Projects/STUDIA/AlgDataStructsC/src/Tables.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_Tables.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_Tables.c$(DependSuffix): ../../../src/Tables.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_Tables.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_Tables.c$(DependSuffix) -MM "../../../src/Tables.c"

$(IntermediateDirectory)/src_Tables.c$(PreprocessSuffix): ../../../src/Tables.c
	@$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_Tables.c$(PreprocessSuffix) "../../../src/Tables.c"

$(IntermediateDirectory)/src_diag.c$(ObjectSuffix): ../../../src/diag.c $(IntermediateDirectory)/src_diag.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/mnt/windows/Users/Adrian/Documents/Visual Studio 2013/Projects/STUDIA/AlgDataStructsC/src/diag.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_diag.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_diag.c$(DependSuffix): ../../../src/diag.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_diag.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_diag.c$(DependSuffix) -MM "../../../src/diag.c"

$(IntermediateDirectory)/src_diag.c$(PreprocessSuffix): ../../../src/diag.c
	@$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_diag.c$(PreprocessSuffix) "../../../src/diag.c"


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) Debug/*$(ObjectSuffix)
	$(RM) Debug/*$(DependSuffix)
	$(RM) $(OutputFile)
	$(RM) ".build-debug/AlgDataStructsC"


