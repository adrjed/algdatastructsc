#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=MinGW-Windows
CND_DLIB_EXT=dll
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/1386528437/AlgDataStructsC.o \
	${OBJECTDIR}/_ext/1386528437/Cw1.o \
	${OBJECTDIR}/_ext/1386528437/Cw2.o \
	${OBJECTDIR}/_ext/1386528437/Cw3.o \
	${OBJECTDIR}/_ext/1386528437/Graphs.o \
	${OBJECTDIR}/_ext/1386528437/Lab1.o \
	${OBJECTDIR}/_ext/1386528437/Lab2.o \
	${OBJECTDIR}/_ext/1386528437/Lab3.o \
	${OBJECTDIR}/_ext/1386528437/Lab4.o \
	${OBJECTDIR}/_ext/1386528437/Lab6.o \
	${OBJECTDIR}/_ext/1386528437/LabLista.o \
	${OBJECTDIR}/_ext/1386528437/Lists.o \
	${OBJECTDIR}/_ext/1386528437/ListsTests.o \
	${OBJECTDIR}/_ext/1386528437/ONP.o \
	${OBJECTDIR}/_ext/1386528437/OnpTests.o \
	${OBJECTDIR}/_ext/1386528437/Queue.o \
	${OBJECTDIR}/_ext/1386528437/Stack.o \
	${OBJECTDIR}/_ext/1386528437/Tables.o \
	${OBJECTDIR}/_ext/1386528437/TreeFS.o \
	${OBJECTDIR}/_ext/1386528437/Trees.o \
	${OBJECTDIR}/_ext/1386528437/ajextra.o \
	${OBJECTDIR}/_ext/1386528437/command.o \
	${OBJECTDIR}/_ext/1386528437/diag.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/algdatastructsc.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/algdatastructsc.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.c} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/algdatastructsc ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/_ext/1386528437/AlgDataStructsC.o: ../../../src/AlgDataStructsC.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1386528437
	${RM} "$@.d"
	$(COMPILE.c) -O2 -std=c11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1386528437/AlgDataStructsC.o ../../../src/AlgDataStructsC.c

${OBJECTDIR}/_ext/1386528437/Cw1.o: ../../../src/Cw1.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1386528437
	${RM} "$@.d"
	$(COMPILE.c) -O2 -std=c11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1386528437/Cw1.o ../../../src/Cw1.c

${OBJECTDIR}/_ext/1386528437/Cw2.o: ../../../src/Cw2.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1386528437
	${RM} "$@.d"
	$(COMPILE.c) -O2 -std=c11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1386528437/Cw2.o ../../../src/Cw2.c

${OBJECTDIR}/_ext/1386528437/Cw3.o: ../../../src/Cw3.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1386528437
	${RM} "$@.d"
	$(COMPILE.c) -O2 -std=c11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1386528437/Cw3.o ../../../src/Cw3.c

${OBJECTDIR}/_ext/1386528437/Graphs.o: ../../../src/Graphs.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1386528437
	${RM} "$@.d"
	$(COMPILE.c) -O2 -std=c11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1386528437/Graphs.o ../../../src/Graphs.c

${OBJECTDIR}/_ext/1386528437/Lab1.o: ../../../src/Lab1.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1386528437
	${RM} "$@.d"
	$(COMPILE.c) -O2 -std=c11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1386528437/Lab1.o ../../../src/Lab1.c

${OBJECTDIR}/_ext/1386528437/Lab2.o: ../../../src/Lab2.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1386528437
	${RM} "$@.d"
	$(COMPILE.c) -O2 -std=c11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1386528437/Lab2.o ../../../src/Lab2.c

${OBJECTDIR}/_ext/1386528437/Lab3.o: ../../../src/Lab3.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1386528437
	${RM} "$@.d"
	$(COMPILE.c) -O2 -std=c11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1386528437/Lab3.o ../../../src/Lab3.c

${OBJECTDIR}/_ext/1386528437/Lab4.o: ../../../src/Lab4.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1386528437
	${RM} "$@.d"
	$(COMPILE.c) -O2 -std=c11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1386528437/Lab4.o ../../../src/Lab4.c

${OBJECTDIR}/_ext/1386528437/Lab6.o: ../../../src/Lab6.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1386528437
	${RM} "$@.d"
	$(COMPILE.c) -O2 -std=c11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1386528437/Lab6.o ../../../src/Lab6.c

${OBJECTDIR}/_ext/1386528437/LabLista.o: ../../../src/LabLista.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1386528437
	${RM} "$@.d"
	$(COMPILE.c) -O2 -std=c11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1386528437/LabLista.o ../../../src/LabLista.c

${OBJECTDIR}/_ext/1386528437/Lists.o: ../../../src/Lists.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1386528437
	${RM} "$@.d"
	$(COMPILE.c) -O2 -std=c11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1386528437/Lists.o ../../../src/Lists.c

${OBJECTDIR}/_ext/1386528437/ListsTests.o: ../../../src/ListsTests.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1386528437
	${RM} "$@.d"
	$(COMPILE.c) -O2 -std=c11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1386528437/ListsTests.o ../../../src/ListsTests.c

${OBJECTDIR}/_ext/1386528437/ONP.o: ../../../src/ONP.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1386528437
	${RM} "$@.d"
	$(COMPILE.c) -O2 -std=c11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1386528437/ONP.o ../../../src/ONP.c

${OBJECTDIR}/_ext/1386528437/OnpTests.o: ../../../src/OnpTests.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1386528437
	${RM} "$@.d"
	$(COMPILE.c) -O2 -std=c11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1386528437/OnpTests.o ../../../src/OnpTests.c

${OBJECTDIR}/_ext/1386528437/Queue.o: ../../../src/Queue.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1386528437
	${RM} "$@.d"
	$(COMPILE.c) -O2 -std=c11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1386528437/Queue.o ../../../src/Queue.c

${OBJECTDIR}/_ext/1386528437/Stack.o: ../../../src/Stack.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1386528437
	${RM} "$@.d"
	$(COMPILE.c) -O2 -std=c11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1386528437/Stack.o ../../../src/Stack.c

${OBJECTDIR}/_ext/1386528437/Tables.o: ../../../src/Tables.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1386528437
	${RM} "$@.d"
	$(COMPILE.c) -O2 -std=c11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1386528437/Tables.o ../../../src/Tables.c

${OBJECTDIR}/_ext/1386528437/TreeFS.o: ../../../src/TreeFS.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1386528437
	${RM} "$@.d"
	$(COMPILE.c) -O2 -std=c11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1386528437/TreeFS.o ../../../src/TreeFS.c

${OBJECTDIR}/_ext/1386528437/Trees.o: ../../../src/Trees.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1386528437
	${RM} "$@.d"
	$(COMPILE.c) -O2 -std=c11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1386528437/Trees.o ../../../src/Trees.c

${OBJECTDIR}/_ext/1386528437/ajextra.o: ../../../src/ajextra.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1386528437
	${RM} "$@.d"
	$(COMPILE.c) -O2 -std=c11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1386528437/ajextra.o ../../../src/ajextra.c

${OBJECTDIR}/_ext/1386528437/command.o: ../../../src/command.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1386528437
	${RM} "$@.d"
	$(COMPILE.c) -O2 -std=c11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1386528437/command.o ../../../src/command.c

${OBJECTDIR}/_ext/1386528437/diag.o: ../../../src/diag.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1386528437
	${RM} "$@.d"
	$(COMPILE.c) -O2 -std=c11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1386528437/diag.o ../../../src/diag.c

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/algdatastructsc.exe

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
